using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class MyTripPlannerApp extends App.AppBase {
    hidden var mView;

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
        mView = new MyTripPlannerView();
        return [ mView, new MyTripPlannerDelegate(mView.method(:onReceive)) ];
    }

}
