using Toybox.WatchUi as Ui;

class MyTripPlannerDelegate extends Ui.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
        notify = handler;
    }

    function onMenu() {
        Ui.pushView(new Rez.Menus.MainMenu(), new MyTripPlannerMenuDelegate(), Ui.SLIDE_UP);
        return true;
    }

}