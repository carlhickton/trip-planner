﻿using GeoJSON.Net.Contrib.EF;
using GeoJSON.Net.Feature;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Text;
using TripPlanner.Services;
using TripPlanner.Services.Models.Convert;
using WebApplication1.Models;
using TrackFileProviderGpx = TripPlanner.Services.Models.Convert.TrackFileProviderGpx;
using TrackFileProviderKml = TripPlanner.Services.Models.Convert.TrackFileProviderKml;
using TrackFileProviderTcx = TripPlanner.Services.Models.Convert.TrackFileProviderTcx;

namespace DataLoader
{
    public class DataLoader
    {
        FirebaseConnection _firebaseClient;
        public DataLoader()
        {
            _firebaseClient = new FirebaseConnection();
        }

        public void LoadFile(string folderName, string source)
        {
            TrackFileProviderTcx tcx = new TrackFileProviderTcx();
            TrackFileProviderGpx gpx = new TrackFileProviderGpx();
            TrackFileProviderKml kml = new TrackFileProviderKml();

            string[] fileEntries = Directory.GetFiles(folderName);

            foreach (var file in fileEntries)
            {
                ICollection<TrackActivity> item = null;

                if (file.EndsWith(".tcx", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = tcx.ReadFile(file);
                }
                else if (file.EndsWith(".gpx", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = gpx.ReadFile(file);
                }
                else if (file.EndsWith(".kml", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = kml.ReadFile(file);
                }

                if (item != null)
                {
                    if (item.Count() == 1 && item.First().Laps.Count == 1)
                    {
                        CreateRoute(item.First(), System.IO.Path.GetFileName(file), source);
                    }

                    if (item.Count() == 1 && item.First().Waypoints.Count != 0)
                    {
                        //await CreateWayPoint(item.First(), source);
                        Console.WriteLine("Is Way Points");
                    }
                }

                Console.WriteLine(file);
            }
        }

        private void CreateWayPoint(TrackActivity item, string source)
        {
            using (var context = new Trip())
            {
                foreach (var w in item.Waypoints)
                {
                    var id = Guid.NewGuid();
                    var geoData = DbGeography.PointFromText(string.Format("POINT({0} {1})", w.Longitude, w.Latitude), DbGeography.DefaultCoordinateSystemId);
                    var feature = new Feature(Utils.GeometryFromDbGeography(geoData));
                    feature.Id = id.ToString();
                    feature.Properties.Add("name", w.Name);
                    feature.Properties.Add("description", w.Description);

                    feature.Properties.Add("link", w.Link);
                    feature.Properties.Add("phone", w.PhoneNumber);
                    feature.Properties.Add("street", w.StreetAddress);
                    feature.Properties.Add("city", w.City);
                    feature.Properties.Add("state", w.State);

                    var icon = source;


                    if (!string.IsNullOrEmpty(w.Icon))
                    {
                        icon = w.Icon.Replace(",", "");
                    }

                    feature.Properties.Add("icon", icon);

                    //var waypoint = new WayPoint()
                    //{
                    //    WayPointId = id,
                    //    Title = w.Name,
                    //    Point = geoData,
                    //    PointType = icon,
                    //    Record = source,
                    //    Json = JsonConvert.SerializeObject(feature)
                    //};
                    //context.WayPoints.Add(waypoint);
                }

                // context.SaveChanges();

            }
        }

        private void CreateRoute(TrackActivity item, string name, string source)
        {
            var sb = new StringBuilder();
            var points = item.Laps.First().Points;
            sb.Append("LINESTRING(");
            for (var i = 0; i < points.Count; i++)
            {
                sb.Append(string.Format("{0} {1}", points[i].Longitude, points[i].Latitude));
                if (i != points.Count - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append(")");

            var id = Guid.NewGuid();
            var geoData = DbGeography.LineFromText(sb.ToString(), DbGeography.DefaultCoordinateSystemId);

            var feature = new Feature(Utils.GeometryFromDbGeography(geoData));
            feature.Properties.Add("name", item.Name);
            feature.Properties.Add("description", item.Description);
            feature.Id = id.ToString();

            using (var context = new Trip())
            {
                var path = new WebApplication1.Models.Path();
                path.PathId = id;
                path.Title = string.IsNullOrEmpty(item.Name) ? name : item.Name;
                //path.Route = geoData;
                //path.Record = source;
                path.Json = JsonConvert.SerializeObject(feature);


                // await _firebaseClient.Child("paths").Child(source).PostAsync(path);

                //context.Paths.Add(path);
                //context.SaveChanges();
            }
        }
    }
}
