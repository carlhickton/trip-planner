using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TripPlanner.Services.Models.Convert;

namespace DataLoader
{
    public class IceLandLoader
    {
        private XmlSerializer _xmlSerializer = new XmlSerializer(typeof(TrackFileProviderTcx.TcxTree));

        public void Load()
        {
            TrackFileProviderKml kml = new TrackFileProviderKml();
            var results = kml.ReadFile(@"C:\dev\trip-planner\data\iceland\iceland2009.kml");

            var laps = results.First().Laps;
            var count = 1;
            foreach (var trackLap in laps)
            {
                CreateTrx(trackLap, count++);
            }
        }

        private void CreateTrx(TrackLap trackLap, int lapnumber)
        {
            var date = DateTime.Now.AddDays(-5);


            var track = new TrackFileProviderTcx.TcxTree();

            track.CourseList = new TrackFileProviderTcx.TcxCourseList();

            var courses = new List<TrackFileProviderTcx.TcxCourse>();

            var course = new TrackFileProviderTcx.TcxCourse();

            course.Name = "Iceland Day " + lapnumber;

            var laps = new List<TrackFileProviderTcx.TcxCourseLap>();
            var lap = new TrackFileProviderTcx.TcxCourseLap();

            lap.BeginPosition = new TrackFileProviderTcx.TcxPosition
            {
                LatitudeDegrees = trackLap.Points.First().Latitude,
                LongitudeDegrees = trackLap.Points.First().Longitude
            };

            lap.EndPosition = new TrackFileProviderTcx.TcxPosition
            {
                LatitudeDegrees = trackLap.Points.Last().Latitude,
                LongitudeDegrees = trackLap.Points.Last().Longitude
            };

            lap.DistanceMeters = GetDistance(trackLap.Points);

            //var points = new List<TrackFileProviderTcx.TcxCoursePoint>();

            //foreach (var trackLapPoint in trackLap.Points)
            //{
            //    points.Add(new TrackFileProviderTcx.TcxCoursePoint
            //    {
            //        Position = new TrackFileProviderTcx.TcxPosition
            //        {
            //            LatitudeDegrees = trackLapPoint.Latitude,
            //            LongitudeDegrees = trackLapPoint.Longitude
            //        },
            //        AltitudeMetersSpecified = false,
            //        Time = date,
            //        AltitudeMeters = 0
            //    });

            //    date = date.AddMinutes(1);
            //}

            course.TrackpointList = new TrackFileProviderTcx.TcxTrackPointList();

            var points = new List<TrackFileProviderTcx.TcxTrackPoint>();

            foreach (var trackLapPoint in trackLap.Points)
            {
                points.Add(new TrackFileProviderTcx.TcxTrackPoint
                {
                    Position = new TrackFileProviderTcx.TcxPosition
                    {
                        LatitudeDegrees = trackLapPoint.Latitude,
                        LongitudeDegrees = trackLapPoint.Longitude
                    },
                    AltitudeMetersSpecified = false,
                    Time = date,
                    AltitudeMeters = 0
                });

                date = date.AddMinutes(1);
            }

            course.TrackpointList.Trackpoints = points.ToArray();

            // course.CoursePoints = points.ToArray();

            //course.CoursePoints = trackLap.Points.Select(x => new TrackFileProviderTcx.TcxCoursePoint
            //{
            //    Position = new TrackFileProviderTcx.TcxPosition
            //    {
            //        LatitudeDegrees = x.Latitude,
            //        LongitudeDegrees = x.Longitude
            //    },
            //    AltitudeMetersSpecified = false,
            //}).ToArray();



            laps.Add(lap);
            course.Laps = laps.ToArray();
            courses.Add(course);
            track.CourseList.Courses = courses.ToArray();


            using (TextWriter writer = new StreamWriter(@"C:\dev\trip-planner\data\iceland\Day" + lapnumber + ".tcx"))
            {
                // Serialize the file
                _xmlSerializer.Serialize(writer, track);

                // Close the writer
                writer.Close();
            }

        }

        private double GetDistance(List<TrackPoint> points)
        {
            double totalDistance = 0;

            GeoCoordinate previous = null;

            foreach (var trackPoint in points)
            {
                var current = new GeoCoordinate(trackPoint.Latitude, trackPoint.Longitude);

                if (previous != null)
                {
                    var distance = previous.GetDistanceTo(current);
                    totalDistance = totalDistance + distance;
                }

                previous = current;
            }


            return totalDistance;
        }
    }
}