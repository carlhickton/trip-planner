namespace WebApplication1.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Path")]
    public partial class Path
    {
        [Key]
        [Column(Order = 0)]
        public Guid PathId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(200)]
        public string Title { get; set; }

        public DbGeography Route { get; set; }

        public string Json { get; set; }

        public string Record { get; set; }
    }

    [Table("WayPoint")]
    public partial class WayPoint
    {
        [Key]
        [Column(Order = 0)]
        public Guid WayPointId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(200)]
        public string Title { get; set; }

        [Column(Order = 2)]
        [StringLength(200)]
        public string Description { get; set; }

        public DbGeography Point { get; set; }

        public string Json { get; set; }

        public string PointType { get; set; }

        public string Record { get; set; }
    }
}
