namespace WebApplication1.Models
{
    using System.Data.Entity;

    public partial class Trip : DbContext
    {
        public Trip()
            : base("name=Trip")
        {
        }

        public virtual DbSet<Path> Paths { get; set; }

        public virtual DbSet<WayPoint> WayPoints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
