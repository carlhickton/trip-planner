﻿using GeoJSON.Net.Feature;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using TripPlanner.Services.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/waypoint")]
    public class WayPointController : ApiController
    {
        private IWebsiteRepository _websiteRepository;
        public WayPointController(IWebsiteRepository websiteRepository)
        {
            _websiteRepository = websiteRepository;
        }

        [Route("")]
        public HttpResponseMessage Get(double north, double east, double south, double west, string types)
        {
            List<Feature> results = new List<Feature>();

            string bboxWKT = string.Format("POLYGON(({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", east, north, west, south);

            DbGeography polygon = DbGeography.FromText(bboxWKT, 4326);

            using (var context = new Trip())
            {
                //
                var query = context.WayPoints.AsQueryable(); //.Where(x => x.Point.Intersects(polygon)).Select(x => x.Json).ToListAsync();

                if (!string.IsNullOrEmpty(types))
                {
                    var t = types.Split(',');
                    query = query.Where(x => t.Contains(x.PointType));
                }

                var paths = query.Where(x => x.Point.Intersects(polygon)).Select(x => x.Json).ToList();

                foreach (var path in paths)
                {
                    results.Add(JsonConvert.DeserializeObject<Feature>(path));
                }
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, results);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                Public = true,
                MaxAge = new TimeSpan(1, 0, 0, 0)
            };
            return response;
        }

        [Route("types")]
        public async Task<HttpResponseMessage> GetTypes()
        {
            List<string> results = new List<string>();

            using (var context = new Trip())
            {
                var paths = await context.WayPoints.Select(x => x.PointType).Distinct().ToListAsync();

                foreach (var path in paths)
                {
                    results.Add(path);
                }
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, results);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                Public = true,
                MaxAge = new TimeSpan(1, 0, 0, 0)
            };
            return response;
        }
    }

}
