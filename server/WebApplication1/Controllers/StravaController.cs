﻿using System.Threading.Tasks;
using System.Web.Http;
using TripPlanner.Services.Interfaces;

namespace WebApplication1.Controllers
{

    //Application: Trip Planner 
    //Key: u3u4wxmry85amk6wb6zpy56v78mfud88 
    //Secret: hymm5Ub4Sv4ybRBYSqhynp89jEtXfQvY92mnV6JMm6a 
    //Status: active

    [RoutePrefix("api/strava")]
    public class StravaController : ApiController
    {
        private IAuthService _authService;
        private IStravaService _stravaService;
        public StravaController(IAuthService authService, IStravaService stravaService)
        {
            _authService = authService;
            _stravaService = stravaService;
        }

        [Route("auth")]
        [HttpPost]
        public async Task<object> CreateAccessToken([FromBody]AuthRequest request)
        {
            return await _authService.CreateStravaAccessToken(request.Code);
        }

        [Route("routes")]
        [HttpPost]
        public async Task<object> Routes([FromBody]RouteRequest request)
        {
            return await _stravaService.Routes(request.Token, request.AthleteId);
        }

        [Route("device")]
        [HttpPost]
        public async Task<object> Device([FromBody]DeviceRequest request)
        {
            return await _stravaService.AddRouteToDevice(request.Token, request.RouteId, request.DeviceId);
        }

        public class AuthRequest
        {
            public string Code { get; set; }
        }

        public class RouteRequest
        {
            public string Token { get; set; }

            public long AthleteId { get; set; }
        }

        public class DeviceRequest
        {
            public string Token { get; set; }

            public long RouteId { get; set; }
             
            public string DeviceId { get; set; }
        }
    }
}