﻿using GeoJSON.Net.Feature;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{


    [RoutePrefix("api/demo")]
    public class DemoController : ApiController
    {
        [Route("")]
        public FeatureCollection Get()
        {
            List<Feature> results = new List<Feature>();

            using (var context = new Trip())
            {
                var paths = context.Paths.ToList();

                foreach (var path in paths)
                {
                    //var geoData = Utils.GeometryFromDbGeography(path.Route);
                    //var feature = new Feature(geoData);
                    //feature.Properties.Add("name", path.Title);
                    //feature.Id = path.PathId.ToString();
                    results.Add(JsonConvert.DeserializeObject<Feature>(path.Json));
                }
            }

            return new FeatureCollection(results);
        }

        [Route("waypoints")]
        public FeatureCollection GetWayPoints()
        {
            List<Feature> results = new List<Feature>();

            using (var context = new Trip())
            {
                var paths = context.WayPoints.ToList();

                foreach (var path in paths)
                {
                    //var geoData = Utils.GeometryFromDbGeography(path.Route);
                    //var feature = new Feature(geoData);
                    //feature.Properties.Add("name", path.Title);
                    //feature.Id = path.PathId.ToString();
                    results.Add(JsonConvert.DeserializeObject<Feature>(path.Json));
                }
            }

            return new FeatureCollection(results);
        }
    }
}
