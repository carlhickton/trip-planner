﻿using GeoJSON.Net.Feature;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using TripPlanner.Services;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/path")]
    public class PathController : ApiController
    {
        private readonly IMapConvertService _mapConvertService;
        private readonly IRoutingService _routingService;

        public PathController(IMapConvertService mapConvertService, IRoutingService routingService)
        {
            _mapConvertService = mapConvertService;
            _routingService = routingService;
        }

        [Route("")]
        //public async Task<HttpResponseMessage> Get(double north, double east, double south, double west)
        public async Task<HttpResponseMessage> Get(string source)
        {
            List<Feature> results = new List<Feature>();

            using (var context = new Trip())
            {
                var paths = await context.Paths.Where(x => x.Record == source).Select(x => x.Json).ToListAsync();

                foreach (var path in paths)
                {
                    results.Add(JsonConvert.DeserializeObject<Feature>(path));
                }
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, results);
            response.Headers.CacheControl = new CacheControlHeaderValue
            {
                Public = true,
                MaxAge = new TimeSpan(1, 0, 0, 0)
            };
            return response;
        }

        [Route("parse")]
        [HttpPost]
        public async Task<HttpResponseMessage> ParseNewRoute(ParseNewRouteRequest request)
        {
            var result = await _mapConvertService.ParseRef(request.ObjectRef);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("feature")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateFeature(CreateFeatureRequest request)
        {
            var result = await _routingService.CreateFeature(request.Points, request.Source);
            if (result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Route Not Found");
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [Route("device")]
        [HttpPost]
        public async Task<HttpResponseMessage> Device([FromBody]PathDeviceRequest request)
        {
            var result = await _routingService.AddRouteToDevice(request.DeviceId, request.Name, request.ObjectRef);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public class PathDeviceRequest
        {
            public string DeviceId { get; set; }

            public string Name { get; set; }

            public string ObjectRef { get; set; }
        }

        public class ParseNewRouteRequest
        {
            public string ObjectRef { get; set; }
        }

        public class CreateFeatureRequest
        {
            public List<CreateFeatureCoordsRequest> Points { get; set; }

            public string Source { get; set; }
        }
    }

}
