﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using TripPlanner.Services.Interfaces;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/sync")]
    public class SyncController : ApiController
    {
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IGarminService _garminService;

        public SyncController(IWebsiteRepository websiteRepository, IGarminService garminService)
        {
            _websiteRepository = websiteRepository;
            _garminService = garminService;
        }



        [Route("key")]
        [HttpGet]
        public HttpResponseMessage GetKey()
        {
            var result = _garminService.CreateDeviceKey();

            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                key = result
            });
        }

        [Route("fit")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetFit(string device)
        {

            var activeDevice = await _websiteRepository.GetDevice(device);

            if (activeDevice != null)
            {
                if (activeDevice.Routes != null && activeDevice.Routes.Count != 0)
                {
                    var key = activeDevice.Routes.Keys.First();
                    var stream = _garminService.CreateFitFile(activeDevice.Routes[key]);
                    await _websiteRepository.RemoveDeviceRoutes(device, key);

                    await _websiteRepository.UpdateDeviceSync(device);

                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(stream)
                    };

                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    return result;
                }

                await _websiteRepository.UpdateDeviceSync(device);

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    done = true
                });
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "Device Not Found");
        }
    }

}
