﻿namespace WebApplication1.Controllers
{
    public class CoordinateEntity
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}