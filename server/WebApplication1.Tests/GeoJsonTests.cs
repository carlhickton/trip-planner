﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoJSON.Net.Geometry;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using TripPlanner.Services;
using TripPlanner.Services.Models.Convert;

namespace WebApplication1.Tests
{
    [TestClass]
    public class GeoJsonTests
    {
        const string AuthJson = @"
{
  ""type"": ""service_account"",
  ""project_id"": ""mytripplanner-9edd8"",
  ""private_key_id"": ""51f71877bbf50b3ed957628f2d31d742c8b6f6c3"",
  ""private_key"": ""-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHeuZ2Rsfc5xXG\n6yKyzEywokTaypqNRk1Wg8EaJTINvoWVscbW2YPoh/AcOooKuLzx6O5JOki0F+Fw\nmJtaKPXq4Wi2O9o2wttH/JCJn7C/xB+GzURvmb5yPRI0taLQW9t+c/Qxg7jhoe3J\nHsCw5S4WQk6arVOvxfM1oEyzOV2JaDGzXGzM7B9VAdchWXqIszX9cYixtvOcgCWH\nlczpPhUZcIL6IubtS9V5ccQ10uV/LHndKdcM7MYq8yjD6YVh35UAJsTrZGNToCqN\nGjApzAMoLWzBhoCvz3ezrqz3ZG38o8kywDoVr1hgyUfvmEidDAfqf4qoXEJxucOD\nKb4QYbUJAgMBAAECggEAUER45dD3bUEjNya4kJ+6oDy7Xhq2c/B9ADoGAsJC0NsA\nH8soSp2+AVxp1sILd+L8TI9BCwBD7wvI8CjIIlBnG/tVyjZtSSnBWMcdVfOHCLMf\nSHvidNG1m3xNjh4khJjhypFYCnCJfWlOKGaSfNvLXrORs5XZHdqWUdbN5jRIGhi4\nOA42eq0CQEURrZbj9goZABXPlv4Ikbwp0IXxqkIxT6MChrz+zwCI9jbT5526ND32\nSUMq+Yb5OniLKoy6jVpnlcw621sad+DbWZH79CXEcw64R7j9QvbOx37gkDcwgDZi\nZCVOSwRzYEvpmekWCW7AWI937ggzHRfPYeMmLHj6cQKBgQDmq38kzaOXxCLALszY\nKUfJsF142/dy/LNSCMXVoLY5Y7bn8I9SjNp5yz7ABVE4fEjmZTW8TlleCT2NeSpe\nkMZpnT6kiI0k+jDLDxokB88IDbpl6LR2Uuj6jaCZXB+cjds4VqBmIkkG2RfeZ25e\ns3hOIwKTOAJHeK2IwmC1w37fhQKBgQDdYptt3iV8V1dFSQnx3AkzaZ7iWM4FBOhe\n/0Gj+G80fOI7AJKLl2J6B2obJMsCUAO2gFEt0C0eq3GKSwKDDFgsMKvp1U3LfSL2\nRtaSQIzwdKug8Q7M+2LxdmsJS8316P5074Dy098suJ3fK7OY/AsD51m0JATkPBbk\nwY6GH+i8tQKBgQDRr34hv4mB8brBRwnRSh1G+7rMnfDgJ1T/MphcbP6ZRsrDJwuy\nN/Z2lsqRFYRSu7AtOD3T0sksUWwRyd45HpQu99E1Eih0P7bRMAZMUIjjwk76SsKZ\neR7ufIZMQtE9qDpqiVy/21TVE829z/olvjGTngG+QEDaJZpiYog1UuRnXQKBgB+d\naFLdSX8jPexADPKEGLBF4hGy2uDunXyLeUVAQik9uFTytPrISKDilC2CKJeQ86bJ\nVk5zjy31D8bPBXHPfnv5C2PeOTjZCvgej5/dJgNktenK4vE06SFQZuKx95yphEUQ\nutpylUlep/Tp2cD8JVqOXEoUZrptNlu+pGrU/biNAoGBAK/pnO9vl2XOMBwQaXZR\n21NP+hfWPllZhtZNhlmGvzn4k15I9dYTk+gP9A0pEgt1rZTQuABApe5nGM5PIyDc\njtyWrD653hgz4RM60lffFhMgnTivwfl+Yw63N20IzYstRdx3ZYUcQy86/bVyC1NR\n6H3UVvU8Mwbngz2UXNruxfN8\n-----END PRIVATE KEY-----\n"",
  ""client_email"": ""firebase-adminsdk-7iq0c@mytripplanner-9edd8.iam.gserviceaccount.com"",
  ""client_id"": ""101445395874097063930"",
  ""auth_uri"": ""https://accounts.google.com/o/oauth2/auth"",
  ""token_uri"": ""https://accounts.google.com/o/oauth2/token"",
  ""auth_provider_x509_cert_url"": ""https://www.googleapis.com/oauth2/v1/certs"",
  ""client_x509_cert_url"": ""https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-7iq0c%40mytripplanner-9edd8.iam.gserviceaccount.com""
}";

        [TestMethod]
        public void Test1()
        {
            var coordinates = new List<IPosition>
            {
                new GeographicPosition(52.370725881211314, 4.889259338378906),
                new GeographicPosition(52.3711451105601, 4.895267486572266),
                new GeographicPosition(52.36931095278263, 4.892091751098633),
                new GeographicPosition(52.370725881211314, 4.889259338378906)
            };

            var lineString = new LineString(coordinates);

            Assert.IsTrue(lineString.IsClosed());
        }

        [TestMethod]
        public void Test2()
        {
            GoogleCredential credential = GoogleCredential.FromJson(AuthJson);

            string projectId = "mytripplanner-9edd8";

            // Instantiates a client
            StorageClient storageClient = StorageClient.Create(credential);

            // The name for the new bucket
            //string bucketName = "my-new-bucket";

            // Creates the new bucket
            var buckets = storageClient.ListBuckets(projectId);

            foreach (var bucket in buckets)
            {
                Console.WriteLine($"Bucket {bucket.Name} created.");
            }

            var buckeyt = storageClient.GetBucket("mytripplanner-9edd8.appspot.com", new GetBucketOptions());

            var items = storageClient.ListObjects("mytripplanner-9edd8.appspot.com");

            foreach (var item in items)
            {
                Console.WriteLine(item.Name);
            }

            //Console.WriteLine($"Bucket {bucket.Name} created.");
        }

        [TestMethod]
        public async Task Test3()
        {

            GoogleCredential credential = GoogleCredential.FromJson(AuthJson);
            StorageClient storageClient = StorageClient.Create(credential);

            var file = await storageClient.GetObjectAsync("mytripplanner-9edd8.appspot.com",
                "users/userid/stage-15-route.kml");

            string value;

            TrackFileProviderKml kml = new TrackFileProviderKml();

            using (var s = new MemoryStream())
            {
                await storageClient.DownloadObjectAsync(file, s);

                s.Position = 0;

                using (var reader = new StreamReader(s, Encoding.UTF8))
                {
                    value = reader.ReadToEnd();
                }
            }


            var trackActivity = kml.ReadFromString(value).ToList();


            Console.WriteLine(trackActivity.Count);

            Assert.Fail("still wroting");
        }

        [TestMethod]
        public async Task Test4()
        {
            var mapConvertService = new MapConvertService(new FirebaseConnection());

            var result = await mapConvertService.ParseRef("users/kJ5A6nUhX5Z58vmUs7w3x9rGUS13/stage-1-route.kml");

            Assert.IsNotNull(result.GeoJsonRef);
            Assert.IsNotNull(result.SummaryMap);

            Assert.IsTrue(result.SummaryMap.Length < 8000, "Length: " + result.SummaryMap.Length);
        }
    }
}
