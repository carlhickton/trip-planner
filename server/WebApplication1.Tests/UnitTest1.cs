﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication1.Models;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Spatial;
using System.Text;
using System.IO;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Contrib.EF;
using Newtonsoft.Json;
using TripPlanner.Services.Models.Convert;

namespace WebApplication1.Tests
{

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [Ignore]
        public void TestMethod1()
        {
            using (var context = new Trip())
            {
                var path = new Models.Path();
                path.PathId = Guid.NewGuid();
                path.Title = "test";
                context.Paths.Add(path);
                context.SaveChanges();
            }
        }

        [TestMethod]
        [Ignore]
        public void Test2()
        {
            XElement xelement = XElement.Load("..\\..\\Rome to Flerance.tcx");
            //var employees = from nm in xelement.Elements("Courses")
            //           // where (string)nm.Element("Sex") == "Female"
            //           select nm;

            var employees = xelement.Descendants("Position").Select(x => new
            {
                lat = x.Element("LatitudeDegrees").Value,
                lng = x.Element("LongitudeDegrees").Value
            }).ToList();

            // Read the entire XML
            foreach (var employee in employees)
            {
                Console.WriteLine(employee);
            }
        }

        [TestMethod]
        [Ignore]
        public void Test3()
        {
            TrackFileProviderTcx tcx = new TrackFileProviderTcx();

            var items = tcx.ReadFile("..\\..\\Rome to Flerance.tcx");

            var points = items.First().Laps.First().Points;

            var sb = new StringBuilder();

            sb.Append("LINESTRING(");
            for (var i = 0; i < points.Count; i++)
            {
                sb.Append(string.Format("{0} {1}", points[i].Longitude, points[i].Latitude));
                if (i != points.Count - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append(")");

            using (var context = new Trip())
            {
                var path = new Models.Path();
                path.PathId = Guid.NewGuid();
                path.Title = "test3";
                path.Route = DbGeography.LineFromText(sb.ToString(), DbGeography.DefaultCoordinateSystemId);
                context.Paths.Add(path);
                context.SaveChanges();
            }
        }

        [TestMethod]
        [Ignore]
        public void Test4()
        {

            TrackFileProviderTcx tcx = new TrackFileProviderTcx();
            TrackFileProviderGpx gpx = new TrackFileProviderGpx();
            TrackFileProviderKml kml = new TrackFileProviderKml();

            using (var context = new Trip())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Path]");
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [WayPoint]");
            }
            //C:\Temp\gpsdata\Info\Unzip
            //string[] fileEntries = Directory.GetFiles(@"C:\Temp\gpsdata");
            string[] fileEntries = Directory.GetFiles(@"C:\Temp\gpsdata\Info\Unzip");

            foreach (var file in fileEntries)
            {
                ICollection<TrackActivity> item = null;

                if (file.EndsWith(".tcx", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = tcx.ReadFile(file);
                }
                else if (file.EndsWith(".gpx", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = gpx.ReadFile(file);
                }
                else if (file.EndsWith(".kml", StringComparison.InvariantCultureIgnoreCase))
                {
                    item = kml.ReadFile(file);
                }

                if (item != null)
                {
                    if (item.Count() == 1 && item.First().Laps.Count == 1)
                    {
                        CreateRoute(item.First(), System.IO.Path.GetFileName(file));
                    }

                    if (item.Count() == 1 && item.First().Waypoints.Count != 0)
                    {
                        CreateWayPoint(item.First());
                        Console.WriteLine("Is Way Points");
                    }
                    //else
                    //{
                    //    Console.WriteLine("Unknown");
                    //}
                }

                Console.WriteLine(file);
            }


            //
        }

        private void CreateWayPoint(TrackActivity item)
        {
            using (var context = new Trip())
            {
                foreach (var w in item.Waypoints)
                {
                    var id = Guid.NewGuid();
                    var geoData = DbGeography.PointFromText(string.Format("POINT({0} {1})", w.Longitude, w.Latitude), DbGeography.DefaultCoordinateSystemId);
                    var feature = new Feature(Utils.GeometryFromDbGeography(geoData));
                    feature.Id = id.ToString();
                    feature.Properties.Add("name", w.Name);
                    feature.Properties.Add("description", w.Description);
                    if (!string.IsNullOrEmpty(w.Icon))
                    {
                        feature.Properties.Add("icon", w.Icon);
                    }

                    var waypoint = new WayPoint()
                    {
                        WayPointId = id,
                        Title = w.Name,
                        Point = geoData,
                        PointType = w.Icon,
                        Record = "info",
                        Json = JsonConvert.SerializeObject(feature)
                    };
                    context.WayPoints.Add(waypoint);
                }

                context.SaveChanges();

            }
        }

        private void CreateRoute(TrackActivity item, string name)
        {
            var sb = new StringBuilder();
            var points = item.Laps.First().Points;
            sb.Append("LINESTRING(");
            for (var i = 0; i < points.Count; i++)
            {
                sb.Append(string.Format("{0} {1}", points[i].Longitude, points[i].Latitude));
                if (i != points.Count - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append(")");

            var id = Guid.NewGuid();
            var geoData = DbGeography.LineFromText(sb.ToString(), DbGeography.DefaultCoordinateSystemId);
            var geoData2 = Utils.GeometryFromDbGeography(geoData);
            var feature = new Feature(geoData2);
            feature.Properties.Add("name", item.Name);
            feature.Properties.Add("description", item.Description);
            feature.Id = id.ToString();

            using (var context = new Trip())
            {
                var path = new Models.Path();
                path.PathId = id;
                path.Title = string.IsNullOrEmpty(item.Name) ? name: item.Name;
                path.Route = geoData;
                path.Record = "info";
                path.Json = JsonConvert.SerializeObject(feature);
                context.Paths.Add(path);
                context.SaveChanges();
            }
        }
    }
}
