﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GeoJSON.Net.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace WebApplication1.Tests
{
    [TestClass]
    public class CreateRouteTests
    {
        [TestMethod]
        public async Task TestMethod1()
        {
            //https://graphhopper.com/api/1/route?point=41.883467267314714,12.488021850585938&point=41.98562754955871,12.551193237304688&vehicle=bike&locale=en&instructions=false&points_encoded=false&key=cdbdebe8-5ede-456d-a82d-2cd744611b51&elevation=true

            //

            var sb = new StringBuilder();

            sb.Append("https://api.mapbox.com/directions/v5/mapbox/cycling/41.88857916758014%2C12.492828369140625%3B42.145078043817534%2C12.2772216796875.json");
            sb.Append("?access_token=pk.eyJ1IjoiY2FybGhpY2t0b24iLCJhIjoiY2oxeGRtbm4xMDE3YTJ2cG12dWQzY3Z0NSJ9.L9mECxYytHlJ5rItre9tvQ");
            sb.Append("&geometries=geojson");
            sb.Append("&overview=full");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(sb.ToString()),
                Method = HttpMethod.Get
            };

            MapBoxResponse result = null;

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var s = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<MapBoxResponse>(s);
                }
            }

            Assert.IsNotNull(result);
        }

        public class MapBoxResponse
        {
            [JsonProperty("routes")]
            public List<MapBoxRouteResponse> Routes { get; set; }
        }

        public class MapBoxRouteResponse
        {
            [JsonProperty("geometry")]
            public LineString Geometry { get; set; }

            [JsonProperty("distance")]
            public double Distance { get; set; }
        }




    }
}
