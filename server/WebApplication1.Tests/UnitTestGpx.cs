﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TripPlanner.Services.Models.Convert;

namespace WebApplication1.Tests
{
    [TestClass]
    public class UnitTestGpx2
    {

        [TestMethod]
        public void TestMethod1()
        {
            TrackFileProviderTcx tcx = new TrackFileProviderTcx();
        }
    }

    [TestClass]
    public class UnitTestGpx
    {
        TrackFileProviderGpx gpx = new TrackFileProviderGpx();

        [TestMethod]
        public void TestMethod1()
        {
            var wayPoints = gpx.ReadFile(@"waypoints.gpx");

            var waypoint = wayPoints.First().Waypoints.First();

            Assert.AreEqual("Agriturismo Le Matinelle", waypoint.Name);
            Assert.AreEqual((float)40.696694450452924, waypoint.Latitude);
            Assert.AreEqual((float)16.529611106961966, waypoint.Longitude);
            Assert.AreEqual("Campground", waypoint.Icon);
            Assert.AreEqual("0835 307343 lematinelle.com", waypoint.Description);
            Assert.AreEqual("http://www.lematinelle.com", waypoint.Link);
            Assert.AreEqual("0835 307343", waypoint.PhoneNumber);

            Assert.AreEqual("Via Laghetto 1", waypoint.StreetAddress);
            Assert.AreEqual("Astano", waypoint.City);
            Assert.AreEqual("TI", waypoint.State);
        }
    }
}
