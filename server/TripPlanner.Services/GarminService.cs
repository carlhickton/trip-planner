﻿using System;
using Dynastream.Fit;
using GeoJSON.Net.Feature;
using System.Device.Location;
using System.IO;
using GeoJSON.Net.Geometry;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TripPlanner.Services.Interfaces;
using TripPlanner.Services.Models;
using DateTime = Dynastream.Fit.DateTime;

namespace TripPlanner.Services
{
    public class GarminService : IGarminService
    {
        private const double MetersPerSecond = 5.5;
        private const int KeyLength = 8;

        private static GarminCourseDetail CreateCourse(Feature feature)
        {
            var data = feature.Geometry as LineString;

            if (data == null)
            {
                return null;
            }

            var startDateTime = System.DateTime.Now.AddYears(-1);
            var dateTime = System.DateTime.Now.AddYears(-1);

            var coordinates = data.Coordinates.OfType<GeographicPosition>().ToList();

            var name = feature.Properties["name"].ToString();
            var c = new GarminCourseDetail(startDateTime, name);

            GeoCoordinate previous = null;

            c.Lap.SetStartTime(new DateTime(dateTime));

            c.Lap.SetStartPositionLat(ToSemicircles(coordinates.First().Latitude));
            c.Lap.SetStartPositionLong(ToSemicircles(coordinates.First().Longitude));

            c.Lap.SetEndPositionLat(ToSemicircles(coordinates.Last().Latitude));
            c.Lap.SetEndPositionLong(ToSemicircles(coordinates.Last().Longitude));

            double totalDistance = 0;

            double totalAscent = 0;
            double totalDescent = 0;
            var previousAltitude = double.MinValue;

            foreach (var coord in coordinates)
            {
                var record = new RecordMesg();

                record.SetPositionLat(ToSemicircles(coord.Latitude));
                record.SetPositionLong(ToSemicircles(coord.Longitude));

                double altitude = 0;
                if (coord.Altitude.HasValue)
                {
                    Console.WriteLine(coord.Altitude);
                    record.SetAltitude(Convert.ToSingle(coord.Altitude));
                    altitude = coord.Altitude.Value;
                }

                var current = new GeoCoordinate(coord.Latitude, coord.Longitude, altitude);

                if (previous != null)
                {
                    var distance = previous.GetDistanceTo(current);
                    totalDistance = totalDistance + distance;
                    dateTime = dateTime.AddSeconds(distance * MetersPerSecond);

                    record.SetTimestamp(new DateTime(dateTime));
                    record.SetSpeed(Convert.ToSingle(MetersPerSecond));
                    record.SetDistance(Convert.ToSingle(totalDistance));
                }
                else
                {
                    record.SetTimestamp(new DateTime(dateTime));
                    record.SetSpeed(0);
                    record.SetDistance(0);
                }

                if (Math.Abs(previousAltitude - double.MinValue) > 0)
                {
                    if (previousAltitude > altitude)
                    {
                        totalDescent = totalDescent + (previousAltitude - altitude);
                    }
                    else if (previousAltitude < altitude)
                    {
                        totalAscent = totalAscent + (altitude - previousAltitude);
                    }
                }

                previous = current;
                previousAltitude = altitude;
                c.Points.Add(record);
            }

            c.Lap.SetTotalDistance(Convert.ToSingle(totalDistance));
            c.Lap.SetTimestamp(new DateTime(dateTime));
            c.Lap.SetTotalElapsedTime(Convert.ToSingle((dateTime - startDateTime).TotalSeconds));
            c.Lap.SetTotalTimerTime(Convert.ToSingle((dateTime - startDateTime).TotalSeconds));
            c.Lap.SetAvgSpeed(Convert.ToSingle(MetersPerSecond));
            c.Lap.SetMaxSpeed(Convert.ToSingle(MetersPerSecond));
            c.Lap.SetTotalAscent(Convert.ToUInt16(totalAscent));
            c.Lap.SetTotalDescent(Convert.ToUInt16(totalDescent));

            return c;
        }

        public Stream CreateFitFile(Feature feature)
        {
            // Generate some FIT messages
            var fileIdMesg = new FileIdMesg(); // Every FIT file MUST contain a 'File ID' message as the first message

            fileIdMesg.SetType(Dynastream.Fit.File.Course);
            fileIdMesg.SetManufacturer(Manufacturer.Garmin);
            fileIdMesg.SetProduct(1001);
            fileIdMesg.SetTimeCreated(new DateTime(System.DateTime.Now));

            var courseDetail = CreateCourse(feature);

            var fitDest = new MemoryStream();

            // Create file encode object
            Encode encodeDemo = new Encode(ProtocolVersion.V20);
            // using (FileStream fitDest = new FileStream("foo.fit", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                // Write our header
                encodeDemo.Open(fitDest);
                encodeDemo.Write(fileIdMesg);

                encodeDemo.Write(courseDetail.Course);
                encodeDemo.Write(courseDetail.Lap);
                encodeDemo.Write(courseDetail.StartEvent);
                encodeDemo.Write(courseDetail.Points);
                encodeDemo.Write(courseDetail.EndEvent);

                // Update header datasize and file CRC
                encodeDemo.Close();
            }

            fitDest.Position = 0;

            return fitDest;
        }

        public string CreateDeviceKey()
        {
            var length = KeyLength + 1;
            const string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var stringBuilder = new StringBuilder();
            using (var rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    if (length == 4)
                    {
                        stringBuilder.Append("-");
                    }
                    else
                    {
                        rng.GetBytes(uintBuffer);
                        var num = BitConverter.ToUInt32(uintBuffer, 0);
                        stringBuilder.Append(valid[(int)(num % (uint)valid.Length)]);
                    }
                    
                }
            }

            return stringBuilder.ToString();
        }

        private static int ToSemicircles(double value)
        {
            var result = value * (Math.Pow(2, 31) / 180);
            return Convert.ToInt32(result);
        }
    }
}
