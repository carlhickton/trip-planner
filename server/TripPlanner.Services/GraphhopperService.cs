using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json;
using TripPlanner.Services.Interfaces;

namespace TripPlanner.Services
{
    public class RoutingService : IRoutingService
    {
        private readonly IFirebaseConnection _firebaseConnection;

        private IWebsiteRepository _websiteRepository;

        public RoutingService(IFirebaseConnection firebaseConnection, IWebsiteRepository websiteRepository)
        {
            _firebaseConnection = firebaseConnection;
            _websiteRepository = websiteRepository;
        }

        public async Task<List<Feature>> CreateFeature(List<CreateFeatureCoordsRequest> points, string source)
        {
            if (source.Equals("cycle-map"))
            {
                return await CreateMapBoxFeature(points);
            }

            return new List<Feature> {await CreateGraphhopperFeature(points, source)};
        }

        public async Task<bool> AddRouteToDevice(string deviceId, string name, string objectRef)
        {
            var storageClient = _firebaseConnection.GetStorageClient();

            var file = await storageClient.GetObjectAsync(_firebaseConnection.GetStorageBucket, objectRef);

            string value;

            using (var s = new MemoryStream())
            {
                await storageClient.DownloadObjectAsync(file, s);

                s.Position = 0;

                using (var reader = new StreamReader(s, Encoding.UTF8))
                {
                    value = reader.ReadToEnd();
                }
            }

            var feature = JsonConvert.DeserializeObject<Feature>(value);

            feature.Properties["name"] = name;

            await _websiteRepository.AddRouteToDevice(deviceId, feature);

            return true;
        }

        private async Task<List<Feature>> CreateMapBoxFeature(List<CreateFeatureCoordsRequest> points)
        {
            var pointsUrl = string.Empty;
            foreach (var point in points)
            {
                pointsUrl = pointsUrl + $"{point.Longitude},{point.Latitude};";
            }

            pointsUrl = pointsUrl.Substring(0, pointsUrl.Length - 1);

            var sb = new StringBuilder();

            sb.Append("https://api.mapbox.com/directions/v5/mapbox/cycling/");
            sb.Append(pointsUrl);
            sb.Append(".json");
            sb.Append("?access_token=pk.eyJ1IjoiY2FybGhpY2t0b24iLCJhIjoiY2oxeGRtbm4xMDE3YTJ2cG12dWQzY3Z0NSJ9.L9mECxYytHlJ5rItre9tvQ");
            sb.Append("&geometries=geojson");
            sb.Append("&overview=full");
            sb.Append("&alternatives=true");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(sb.ToString()),
                Method = HttpMethod.Get
            };

            MapBoxResponse result = null;

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var s = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<MapBoxResponse>(s);
                }
            }

            if (result == null)
            {
                return null;
            }

            var results = new List<Feature>();

            foreach (var route in result.Routes)
            {
                var feature = new Feature(route.Geometry);
                feature.Properties["distance"] = route.Distance;
                feature.Properties["elevation"] = 0;
                results.Add(feature);
            }
            

            return results;
        }

        private async Task<Feature> CreateGraphhopperFeature(List<CreateFeatureCoordsRequest> points, string source)
        {
            var sb = new StringBuilder();
            sb.Append("https://graphhopper.com/api/1/route");
            sb.Append("?locale=en&instructions=false&points_encoded=false&elevation=true");

            sb.Append(source.Equals("cycle-road", StringComparison.OrdinalIgnoreCase)
                ? "&vehicle=racingbike"
                : "&vehicle=bike");

            sb.Append("&key=cdbdebe8-5ede-456d-a82d-2cd744611b51");

            foreach (var point in points)
            {
                sb.Append($"&point={point.Latitude},{point.Longitude}");
            }

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(sb.ToString()),
                Method = HttpMethod.Get
            };

            GraphhopperResponse result = null;

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var s = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<GraphhopperResponse>(s);
                }
            }

            if (result == null)
            {
                return null;
            }

            var path = result.Path.First();

            var feature = new Feature(path.Points);
            feature.Properties["descend"] = path.Descend;
            feature.Properties["elevation"] = path.Ascend;
            feature.Properties["distance"] = path.Distance;

            return feature;
        }

        internal class MapBoxResponse
        {
            [JsonProperty("routes")]
            public List<MapBoxRouteResponse> Routes { get; set; }
        }

        internal class MapBoxRouteResponse
        {
            [JsonProperty("geometry")]
            public LineString Geometry { get; set; }

            [JsonProperty("distance")]
            public double Distance { get; set; }
        }

        internal class GraphhopperResponse
        {
            [JsonProperty("paths")]
            public List<GraphhopperPath> Path { get; set; }
        }

        internal class GraphhopperPath
        {
            [JsonProperty("descend")]
            public double Descend { get; set; }

            [JsonProperty("ascend")]
            public double Ascend { get; set; }

            [JsonProperty("distance")]
            public double Distance { get; set; }

            [JsonProperty("points")]
            public LineString Points { get; set; }
        }
    }

    public interface IRoutingService
    {
        Task<List<Feature>> CreateFeature(List<CreateFeatureCoordsRequest> points, string source);
        Task<bool> AddRouteToDevice(string deviceId, string name, string objectRef);
    }

    public class CreateFeatureCoordsRequest
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lng")]
        public double Longitude { get; set; }
    }
}