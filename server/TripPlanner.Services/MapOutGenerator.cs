using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json;
using TripPlanner.Services.Interfaces;
using TripPlanner.Services.Models.Convert;

namespace TripPlanner.Services
{
    public class MapOutGenerator : IMapOutGenerator
    {
        private readonly IFirebaseConnection _firebaseConnection;
        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(TrackFileProviderTcx.TcxTree));

        public MapOutGenerator(IFirebaseConnection firebaseConnection)
        {
            _firebaseConnection = firebaseConnection;
        }

        public async Task<string> ParseRef(string objectRef)
        {
            var storageClient = _firebaseConnection.GetStorageClient();

            var file = await storageClient.GetObjectAsync(_firebaseConnection.GetStorageBucket, objectRef);

            string value;

            using (var s = new MemoryStream())
            {
                await storageClient.DownloadObjectAsync(file, s);

                s.Position = 0;

                using (var reader = new StreamReader(s, Encoding.UTF8))
                {
                    value = reader.ReadToEnd();
                }
            }

            var feature = JsonConvert.DeserializeObject<Feature>(value);
            var line = feature.Geometry as LineString;
            if (line == null)
            {
                return null;
            }

            var trackLap = new TrackLap();
            trackLap.Points = line.Coordinates.OfType<GeographicPosition>()
                .Select(x => new TrackPoint
                {
                    Longitude = Convert.ToSingle(x.Longitude),
                    Latitude = Convert.ToSingle(x.Latitude),
                    Altitude = Convert.ToSingle(x.Altitude.GetValueOrDefault(0))
                }).ToList();

            CreateTrx(trackLap);

            return "";
        }

        private void CreateTrx(TrackLap trackLap, string name)
        {
            var date = DateTime.Now.AddDays(-5);


            var track = new TrackFileProviderTcx.TcxTree();

            track.CourseList = new TrackFileProviderTcx.TcxCourseList();

            var courses = new List<TrackFileProviderTcx.TcxCourse>();

            var course = new TrackFileProviderTcx.TcxCourse();

            course.Name = name;

            var laps = new List<TrackFileProviderTcx.TcxCourseLap>();
            var lap = new TrackFileProviderTcx.TcxCourseLap();

            lap.BeginPosition = new TrackFileProviderTcx.TcxPosition
            {
                LatitudeDegrees = trackLap.Points.First().Latitude,
                LongitudeDegrees = trackLap.Points.First().Longitude
            };

            lap.EndPosition = new TrackFileProviderTcx.TcxPosition
            {
                LatitudeDegrees = trackLap.Points.Last().Latitude,
                LongitudeDegrees = trackLap.Points.Last().Longitude
            };

            lap.DistanceMeters = GetDistance(trackLap.Points);

            //var points = new List<TrackFileProviderTcx.TcxCoursePoint>();

            //foreach (var trackLapPoint in trackLap.Points)
            //{
            //    points.Add(new TrackFileProviderTcx.TcxCoursePoint
            //    {
            //        Position = new TrackFileProviderTcx.TcxPosition
            //        {
            //            LatitudeDegrees = trackLapPoint.Latitude,
            //            LongitudeDegrees = trackLapPoint.Longitude
            //        },
            //        AltitudeMetersSpecified = false,
            //        Time = date,
            //        AltitudeMeters = 0
            //    });

            //    date = date.AddMinutes(1);
            //}

            course.TrackpointList = new TrackFileProviderTcx.TcxTrackPointList();

            var points = new List<TrackFileProviderTcx.TcxTrackPoint>();

            foreach (var trackLapPoint in trackLap.Points)
            {
                points.Add(new TrackFileProviderTcx.TcxTrackPoint
                {
                    Position = new TrackFileProviderTcx.TcxPosition
                    {
                        LatitudeDegrees = trackLapPoint.Latitude,
                        LongitudeDegrees = trackLapPoint.Longitude
                    },
                    AltitudeMetersSpecified = false,
                    Time = date,
                    AltitudeMeters = 0
                });

                date = date.AddMinutes(1);
            }

            course.TrackpointList.Trackpoints = points.ToArray();

            // course.CoursePoints = points.ToArray();

            //course.CoursePoints = trackLap.Points.Select(x => new TrackFileProviderTcx.TcxCoursePoint
            //{
            //    Position = new TrackFileProviderTcx.TcxPosition
            //    {
            //        LatitudeDegrees = x.Latitude,
            //        LongitudeDegrees = x.Longitude
            //    },
            //    AltitudeMetersSpecified = false,
            //}).ToArray();



            laps.Add(lap);
            course.Laps = laps.ToArray();
            courses.Add(course);
            track.CourseList.Courses = courses.ToArray();


            using (TextWriter writer = new StreamWriter(@"C:\dev\trip-planner\data\iceland\" + name + ".tcx"))
            {
                // Serialize the file
                _xmlSerializer.Serialize(writer, track);

                // Close the writer
                writer.Close();
            }

        }

        private double GetDistance(List<TrackPoint> points)
        {
            double totalDistance = 0;

            GeoCoordinate previous = null;

            foreach (var trackPoint in points)
            {
                var current = new GeoCoordinate(trackPoint.Latitude, trackPoint.Longitude);

                if (previous != null)
                {
                    var distance = previous.GetDistanceTo(current);
                    totalDistance = totalDistance + distance;
                }

                previous = current;
            }


            return totalDistance;
        }
    }
}