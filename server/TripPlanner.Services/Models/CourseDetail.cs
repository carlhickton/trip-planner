using System.Collections.Generic;
using Dynastream.Fit;

namespace TripPlanner.Services.Models
{
    internal class GarminCourseDetail
    {
        private readonly System.DateTime _startDateTime;
        private readonly string _courseName;
        public GarminCourseDetail(System.DateTime startDateTime, string courseName)
        {
            _startDateTime = startDateTime;
            _courseName = courseName;
            Lap = new LapMesg();
            Points = new List<RecordMesg>();
        }

        public LapMesg Lap { get; }

        public CourseMesg Course
        {
            get
            {
                var course = new CourseMesg();
                course.SetSport(Sport.Cycling);
                course.SetSubSport(SubSport.Road);
                course.SetName(_courseName);
                return course;
            }
        }

        public EventMesg StartEvent
        {
            get
            {
                var startEvent = new EventMesg();
                startEvent.SetTimestamp(new DateTime(_startDateTime));
                startEvent.SetEvent(Event.Timer);
                startEvent.SetEventType(EventType.Start);
                startEvent.SetEventGroup(0);
                return startEvent;
            }
        }

        public EventMesg EndEvent
        {
            get
            {
                var endEvent = new EventMesg();
                endEvent.SetTimestamp(new DateTime(System.DateTime.Now));
                endEvent.SetEvent(Event.Timer);
                endEvent.SetEventType(EventType.StopDisableAll);
                endEvent.SetEventGroup(0);
                return endEvent;
            }
        }

        public List<RecordMesg> Points { get; }
    }
}