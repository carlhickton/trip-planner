﻿using Newtonsoft.Json;
using System.Collections.Generic;
using GeoJSON.Net.Feature;

namespace TripPlanner.Services.Models
{
    public class Device
    {
        [JsonProperty("deviceId")]
        public string DeviceId { get; set; }

        [JsonProperty("deviceType")]
        public string DeviceType { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("syncDate")]
        public string SyncDate { get; set; }

        [JsonProperty("routes")]
        public Dictionary<string, Feature> Routes { get; set; }
    }
}
