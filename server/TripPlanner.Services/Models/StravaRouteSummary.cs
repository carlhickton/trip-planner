﻿namespace TripPlanner.Services.Models
{
    public class StravaRouteSummary
    {
        public long Id { get; internal set; }
        public string Name { get; internal set; }
        public string Description { get; internal set; }
        public double Distance { get; internal set; }
        public double Elevation { get; internal set; }
        public string SummaryPolyline { get; internal set; }
    }
}
