﻿namespace TripPlanner.Services.Models
{
    public class EncodeCoordinateEntity
    {
        public EncodeCoordinateEntity()
        {
            
        }
        public EncodeCoordinateEntity(double latitude, double longitude): this()
        {
            Latitude = latitude;
            Longitude = longitude;
        }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}