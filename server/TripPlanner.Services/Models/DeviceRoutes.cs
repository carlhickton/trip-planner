﻿using Newtonsoft.Json;

namespace TripPlanner.Services.Models
{
    public class DeviceRoutes
    {
        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("routeData")]
        public string RouteData { get; set; }
    }
}
