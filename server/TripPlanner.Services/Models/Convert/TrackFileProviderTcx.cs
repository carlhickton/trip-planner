﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a track file provider for TCX files.
    /// </summary>
    public partial class TrackFileProviderTcx : ITrackFileProvider
    {
        private XmlSerializer _xmlSerializer = new XmlSerializer(typeof(TrackFileProviderTcx.TcxTree));

        /// <summary>
        /// Determines whether the specified file can be read.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        /// 	<c>true</c> if the specified file can be read; otherwise, <c>false</c>.
        /// </returns>
        public bool CanReadFile(string file)
        {
            return file.EndsWith(".tcx", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Reads the file and return the contained activities.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public ICollection<TrackActivity> ReadFile(string fileName)
        {
            TrackFileProviderTcx.TcxTree tree = ParseFile(fileName);

            return CreateTrackActivities(fileName, tree);
        }

        public ICollection<TrackActivity> ReadFromString(string content)
        {
            TcxTree tree = ParseFileFromString(content);

            return CreateTrackActivities("", tree);
        }

        private ICollection<TrackActivity> CreateTrackActivities(string fileName, TcxTree tree)
        {
            List<TrackActivity> activities = new List<TrackActivity>();

            if (tree == null)
                return activities;

            if (tree.CourseList != null && tree.CourseList.Courses != null)
            {
                foreach (TrackFileProviderTcx.TcxCourse tcxCourse in tree.CourseList.Courses)
                {
                    TrackActivity track = ConvertCourse(tcxCourse);

                    track.FileName = fileName;

                    activities.Add(track);
                }
            }

            if (tree.ActivityList != null && tree.ActivityList.Activities != null)
            {
                foreach (TrackFileProviderTcx.TcxActivity tcxActivity in tree.ActivityList.Activities)
                {
                    TrackActivity track = ConvertActivity(tcxActivity);

                    track.FileName = fileName;

                    activities.Add(track);
                }
            }

            return activities;
        }

        private TrackFileProviderTcx.TcxTree ParseFile(string file)
        {
            TrackFileProviderTcx.TcxTree tree = null;

            try
            {
                using (StreamReader reader = new StreamReader(file))
                {
                    tree = _xmlSerializer.Deserialize(reader) as TrackFileProviderTcx.TcxTree;
                }
            }

            catch (IOException)
            {
                // Error reading file
            }

            catch (InvalidOperationException)
            {
                // Serialization error
            }

            return tree;
        }

        private TrackFileProviderTcx.TcxTree ParseFileFromString(string content)
        {
            TrackFileProviderTcx.TcxTree tree = null;

            try
            {
                using (TextReader reader = new StringReader(content))
                {
                    tree = _xmlSerializer.Deserialize(reader) as TcxTree;
                }
            }

            catch (IOException)
            {
                // Error reading file
            }

            catch (InvalidOperationException)
            {
                // Serialization error
            }

            return tree;
        }

        private static TrackActivity ConvertCourse(TrackFileProviderTcx.TcxCourse tcxCourse)
        {
            TrackActivity track = new TrackActivity();
            track.Laps = new List<TrackLap>();
            track.Waypoints = new List<TrackWaypoint>();

            track.Name = tcxCourse.Name;

            TrackLap lap = new TrackLap();
            lap.Points = new List<TrackPoint>();

            if (tcxCourse.CoursePoints != null)
            {
                foreach (TrackFileProviderTcx.TcxCoursePoint tcxCoursePoint in tcxCourse.CoursePoints)
                {
                    track.Waypoints.Add(ConvertWaypoint(tcxCoursePoint));
                }
            }

            if (tcxCourse.TrackpointList != null && tcxCourse.TrackpointList.Trackpoints != null)
            {
                foreach (TrackFileProviderTcx.TcxTrackPoint tcxPt in tcxCourse.TrackpointList.Trackpoints)
                {
                    TrackPoint pt = ConvertTrackPoint(tcxPt);

                    lap.Points.Add(pt);
                }
            }

            track.Laps.Add(lap);

            return track;
        }

        private static TrackWaypoint ConvertWaypoint(TrackFileProviderTcx.TcxCoursePoint tcxCoursePoint)
        {
            TrackWaypoint waypoint = new TrackWaypoint();

            waypoint.Name = tcxCoursePoint.Name;
            waypoint.Altitude = (float)tcxCoursePoint.AltitudeMeters;
            waypoint.Latitude = (float)tcxCoursePoint.Position.LatitudeDegrees;
            waypoint.Longitude = (float)tcxCoursePoint.Position.LongitudeDegrees;
            waypoint.Time = tcxCoursePoint.Time;

            return waypoint;
        }

        private static TrackActivity ConvertActivity(TrackFileProviderTcx.TcxActivity tcxActivity)
        {
            TrackActivity track = new TrackActivity();
            track.Laps = new List<TrackLap>();
            track.Waypoints = new List<TrackWaypoint>();

            foreach (TrackFileProviderTcx.TcxActivityLap tcxLap in tcxActivity.Laps)
            {
                TrackLap lap = new TrackLap();
                lap.Points = new List<TrackPoint>();

                if (tcxLap.Tracks != null)
                {
                    foreach (TrackFileProviderTcx.TcxTrackPointList tcxTrack in tcxLap.Tracks)
                    {
                        if (tcxTrack.Trackpoints != null)
                        {
                            foreach (TrackFileProviderTcx.TcxTrackPoint tcxPoint in tcxTrack.Trackpoints)
                            {
                                TrackPoint pt = ConvertTrackPoint(tcxPoint);

                                if (pt != null)
                                {
                                    lap.Points.Add(pt);
                                }
                            }
                        }
                    }
                }

                track.Laps.Add(lap);
            }

            return track;
        }

        private static TrackPoint ConvertTrackPoint(TrackFileProviderTcx.TcxTrackPoint tcxPoint)
        {
            if (tcxPoint.Position == null)
                return null;

            TrackPoint pt = new TrackPoint();

            pt.Latitude = (float)tcxPoint.Position.LatitudeDegrees;
            pt.Longitude = (float)tcxPoint.Position.LongitudeDegrees;
            pt.Altitude = (float)tcxPoint.AltitudeMeters;
            pt.Distance = (float)tcxPoint.DistanceMeters;
            pt.Time = tcxPoint.Time;

            return pt;
        }
    }
}