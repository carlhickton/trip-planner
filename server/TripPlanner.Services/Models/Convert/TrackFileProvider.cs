﻿using System.Collections.Generic;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a provider for track files.
    /// </summary>
    interface ITrackFileProvider
    {
        /// <summary>
        /// Determines whether the specified file can be read.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        /// 	<c>true</c> if the specified file can be read; otherwise, <c>false</c>.
        /// </returns>
        bool CanReadFile(string file);

        /// <summary>
        /// Reads the file and return the contained activities.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        ICollection<TrackActivity> ReadFile(string fileName);

        ICollection<TrackActivity> ReadFromString(string content);
    }
}