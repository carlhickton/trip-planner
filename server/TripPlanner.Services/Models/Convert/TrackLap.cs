﻿using System;
using System.Collections.Generic;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a lap of a track.
    /// </summary>
    public class TrackLap
    {
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>The index.</value>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the index of the first point.
        /// </summary>
        /// <value>The index of the first point.</value>
        public int FirstPointIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the last point.
        /// </summary>
        /// <value>The index of the last point.</value>
        public int LastPointIndex { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        /// <value>The points.</value>
        public List<TrackPoint> Points { get; set; }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>The start time.</value>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the total time.
        /// </summary>
        /// <value>The total time.</value>
        public TimeSpan TotalTime { get; set; }

        /// <summary>
        /// Gets or sets the total distance.
        /// </summary>
        /// <value>The total distance.</value>
        public double TotalDistance { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "TrackLap " + Index;
        }
    };
}