﻿using System;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a point of a track.
    /// </summary>
    public class TrackPoint
    {
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>The index.</value>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or sets the altitude.
        /// </summary>
        /// <value>The altitude.</value>
        public float Altitude { get; set; }

        /// <summary>
        /// Gets or sets the distance.
        /// </summary>
        /// <value>The distance.</value>
        public float Distance { get; set; }

        /// <summary>
        /// Gets or sets the accumulated ascent.
        /// </summary>
        /// <value>The accumulated ascent.</value>
        public float AccAsc { get; set; }

        /// <summary>
        /// Gets or sets the accumulated descent.
        /// </summary>
        /// <value>The accumulated descent.</value>
        public float AccDesc { get; set; }

        /// <summary>
        /// Gets or sets the speed.
        /// </summary>
        /// <value>The speed.</value>
        public float Speed { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>The time.</value>
        public DateTime Time { get; set; }

        /// <summary>
        /// Gets or sets the time delta. That is the time since the start of the track.
        /// </summary>
        /// <value>The time delta.</value>
        public TimeSpan TimeDelta { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "TrackPoint " + Index;
        }
    };
}
