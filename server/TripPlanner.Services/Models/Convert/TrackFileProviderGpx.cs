﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a track file provider for GPX files.
    /// </summary>
    public partial class TrackFileProviderGpx : ITrackFileProvider
    {
        private readonly XmlSerializer _xmlSerializer;
        //private XmlSerializerNamespaces ns;

        public TrackFileProviderGpx()
        {
            //ns.Add("gpxx", "http://www.garmin.com/xmlschemas/GpxExtensions/v3");
            _xmlSerializer = new XmlSerializer(typeof(GpxTree));
        }

        /// <summary>
        /// Determines whether the specified file can be read.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        /// 	<c>true</c> if the specified file can be read; otherwise, <c>false</c>.
        /// </returns>
        public bool CanReadFile(string file)
        {
            return file.EndsWith(".gpx", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Reads the file and return the contained activities.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public ICollection<TrackActivity> ReadFile(string fileName)
        {
            GpxTree tree = ParseFile(fileName);

            return CreateTrackActivities(fileName, tree);
        }

        public ICollection<TrackActivity> ReadFromString(string content)
        {
            GpxTree tree = ParseFileFromString(content);

            return CreateTrackActivities("", tree);
        }

        private ICollection<TrackActivity> CreateTrackActivities(string fileName, GpxTree tree)
        {
            List<TrackActivity> activities = new List<TrackActivity>();
            if (tree == null)
                return activities;

            if (tree.Tracks != null)
            {
                foreach (GpxTrack gpxTrack in tree.Tracks)
                {
                    TrackActivity track = ConvertTrack(gpxTrack);

                    track.FileName = fileName;

                    activities.Add(track);
                }
            }

            if (tree.Waypoints != null)
            {
                TrackActivity activity = new TrackActivity
                {
                    FileName = fileName,
                    Laps = new List<TrackLap>(),
                    Waypoints = new List<TrackWaypoint>()
                };

                foreach (GpxWaypoint gpxWaypoint in tree.Waypoints)
                {
                    activity.Waypoints.Add(ConvertWaypoint(gpxWaypoint));
                }

                activities.Add(activity);
            }

            return activities;
        }

        private GpxTree ParseFileFromString(string content)
        {
            GpxTree tree = null;

            try
            {
                using (TextReader reader = new StringReader(content))
                {
                    tree = _xmlSerializer.Deserialize(reader) as GpxTree;
                }
            }

            catch (IOException)
            {
                // Error reading file
            }

            catch (InvalidOperationException)
            {
                // Serialization error
            }

            return tree;
        }

        private GpxTree ParseFile(string file)
        {
            GpxTree tree = null;

            try
            {
                using (StreamReader reader = new StreamReader(file))
                {
                    tree = _xmlSerializer.Deserialize(reader) as GpxTree;
                }
            }

            catch (IOException)
            {
                // Error reading file
            }

            catch (InvalidOperationException)
            {
                // Serialization error
            }

            return tree;
        }

        private static TrackWaypoint ConvertWaypoint(GpxWaypoint gpxWaypoint)
        {
            TrackWaypoint waypoint = new TrackWaypoint
            {
                Altitude = (float) gpxWaypoint.Elevation,
                Latitude = (float) gpxWaypoint.Lat,
                Longitude = (float) gpxWaypoint.Lon,
                Name = gpxWaypoint.Name,
                Description = gpxWaypoint.Description,
                Icon = gpxWaypoint.Icon
            };

            var link = gpxWaypoint.Link.FirstOrDefault();
            if (link != null)
            {
                waypoint.Link = link.Href;
            }

            var extensions = gpxWaypoint.Extensions.SelectMany(x => x.Any).Where(x => x.NamespaceURI == "http://www.garmin.com/xmlschemas/GpxExtensions/v3").ToList();
            if (extensions.Count != 0)
            {
                var firstElement = extensions.First();
                waypoint.PhoneNumber = firstElement.ChildNodes.OfType<XmlElement>().FirstOrDefault(x => x.LocalName == "PhoneNumber")?.InnerText;

                var address = firstElement.ChildNodes.OfType<XmlElement>().FirstOrDefault(x => x.LocalName == "Address");
                if (address != null)
                {
                    waypoint.StreetAddress = address.ChildNodes.OfType<XmlElement>().FirstOrDefault(x => x.LocalName == "StreetAddress")?.InnerText;
                    waypoint.City = address.ChildNodes.OfType<XmlElement>().FirstOrDefault(x => x.LocalName == "City")?.InnerText;
                    waypoint.State = address.ChildNodes.OfType<XmlElement>().FirstOrDefault(x => x.LocalName == "State")?.InnerText;
                }
            }

            return waypoint;
        }

        private static TrackActivity ConvertTrack(GpxTrack gpxTrack)
        {
            TrackActivity track = new TrackActivity
            {
                Laps = new List<TrackLap>(),
                Waypoints = new List<TrackWaypoint>(),
                Name = gpxTrack.Name,
                Description = gpxTrack.Description
            };


            foreach (GpxTrackSegment segment in gpxTrack.TrackSegments)
            {
                TrackLap lap = new TrackLap
                {
                    Points = new List<TrackPoint>()
                };

                foreach (GpxTrackPoint gpxPt in segment.TrackPoints)
                {
                    TrackPoint pt = new TrackPoint
                    {
                        Latitude = (float) gpxPt.Lat,
                        Longitude = (float) gpxPt.Lon,
                        Altitude = (float) gpxPt.Elevation,
                        Time = gpxPt.Time
                    };

                    lap.Points.Add(pt);
                }

                track.Laps.Add(lap);
            }

            return track;
        }
    }
}