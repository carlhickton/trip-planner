﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace TripPlanner.Services.Models.Convert
{
    public partial class TrackFileProviderGpx
    {
        // CA1034: Nested types should not be visible
        // --> XMLSerializer requires the type to be externally visible

        // CA1819: Properties should not return arrays
        // --> XMLSerializer requires arrays

        [XmlRoot("gpx", Namespace = "http://www.topografix.com/GPX/1/1")]
        [SuppressMessage("Microsoft.Design", "CA1034")]
        public class GpxTree
        {
            [XmlAttribute("version")]
            public string Version { get; set; }

            [XmlAttribute("creator")]
            public string Creator { get; set; }

            [XmlElement("wpt")]
            [SuppressMessage("Microsoft.Performance", "CA1819")]
            public GpxWaypoint[] Waypoints { get; set; }

            [XmlElement("trk")]
            [SuppressMessage("Microsoft.Performance", "CA1819")]
            public GpxTrack[] Tracks { get; set; }
        }

        [XmlRoot("wpt")]
        [SuppressMessage("Microsoft.Design", "CA1034")]
        public class GpxWaypoint
        {
            [XmlAttribute(DataType = "double", AttributeName = "lat")]
            public double Lat { get; set; }

            [XmlAttribute(DataType = "double", AttributeName = "lon")]
            public double Lon { get; set; }

            [XmlElement("ele", DataType = "double")]
            public double Elevation { get; set; }

            [XmlElement("name")]
            public string Name { get; set; }

            [XmlAttribute("cmt")]
            public string Comment { get; set; }

            [XmlElement("desc")]
            public string Description { get; set; }

            [XmlElement("sym")]
            public string Icon { get; set; }

            [XmlElement("link")]
            public List<LinkType> Link { get; set; }

            [XmlElement("time")]
            public DateTime Time { get; set; }

            [XmlElement("extensions")]
            public List<ExtensionsType> Extensions { get; set; }
        }

        public class LinkType
        {
            [XmlAttribute("href", DataType = "anyURI")]
            public string Href { get; set; }
        }

        public class ExtensionsType
        {
            [XmlAnyElement()]
            public System.Xml.XmlElement[] Any { get; set; }
        }

        [XmlRoot("trk")]
        [SuppressMessage("Microsoft.Design", "CA1034")]
        public class GpxTrack
        {
            [XmlElement("name")]
            public string Name { get; set; }

            [XmlElement("cmt")]
            public string Comment { get; set; }

            [XmlElement("desc")]
            public string Description { get; set; }

            [XmlElement("trkseg")]
            [SuppressMessage("Microsoft.Performance", "CA1819")]
            public GpxTrackSegment[] TrackSegments { get; set; }
        }

        [XmlRoot("trkseg")]
        [SuppressMessage("Microsoft.Design", "CA1034")]
        public class GpxTrackSegment
        {
            [XmlElement("trkpt")]
            [SuppressMessage("Microsoft.Performance", "CA1819")]
            public GpxTrackPoint[] TrackPoints { get; set; }
        }

        [XmlRoot("trkpt")]
        [SuppressMessage("Microsoft.Design", "CA1034")]
        public class GpxTrackPoint
        {
            [XmlAttribute(DataType = "double", AttributeName = "lat")]
            public double Lat { get; set; }

            [XmlAttribute(DataType = "double", AttributeName = "lon")]
            public double Lon { get; set; }

            [XmlElement("ele")]
            public double Elevation { get; set; }

            [XmlElement("time")]
            public DateTime Time { get; set; }
        }

    }
}
