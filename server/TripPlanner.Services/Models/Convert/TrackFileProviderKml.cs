﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharpKml.Base;
using SharpKml.Dom;

namespace TripPlanner.Services.Models.Convert
{
    /// <summary>
    /// Represents a track file provider for KML files.
    /// </summary>
    public partial class TrackFileProviderKml : ITrackFileProvider
    {
        /// <summary>
        /// Determines whether the specified file can be read.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        /// 	<c>true</c> if the specified file can be read; otherwise, <c>false</c>.
        /// </returns>
        public bool CanReadFile(string file)
        {
            return file.EndsWith(".kml", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Reads the file and return the contained activities.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public ICollection<TrackActivity> ReadFile(string fileName)
        {
            Parser parser = new Parser();
            var xmkl = File.ReadAllLines(fileName);

            parser.ParseString(string.Join("", xmkl), false);

            var root = parser.Root as Kml;

            return TrackActivities(root);
        }

        public ICollection<TrackActivity> ReadFromString(string content)
        {
            Parser parser = new Parser();
            parser.ParseString(content, false);

            var root = parser.Root as Kml;

            return TrackActivities(root);
        }

        private static ICollection<TrackActivity> TrackActivities(Kml root)
        {
            var folder = root.Feature as Document;

            List<TrackActivity> activities = new List<TrackActivity>();

            var f = new TrackActivity();

            f.Waypoints = new List<TrackWaypoint>();
            f.Laps = new List<TrackLap>();

            foreach (var item in folder.Features.ToList())
            {
                var x = item as Placemark;

                var point = x.Geometry as Point;
                var line = x.Geometry as LineString;
                if (point != null)
                {
                    f.Waypoints.Add(new TrackWaypoint
                    {
                        Name = x.Name,
                        Description = x.Description == null ? "" : x.Description.Text,
                        Latitude = (float) point.Coordinate.Latitude,
                        Longitude = (float) point.Coordinate.Longitude,
                    });
                }
                else if (line != null)
                {
                    f.Name = x.Name;
                    f.Description = x.Description == null ? "" : x.Description.Text;
                    f.Laps.Add(new TrackLap
                    {
                        Points = line.Coordinates.Select(y => new TrackPoint
                            {
                                Latitude = (float) y.Latitude,
                                Longitude = (float) y.Longitude
                            })
                            .ToList()
                    });
                }
            }

            activities.Add(f);

            //f.Waypoints = .Select(x => new TrackWaypoint
            //).ToList();

            return activities;
        }
    }
}