﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TripPlanner.Services.Interfaces;

namespace TripPlanner.Services
{
    public class AuthService : IAuthService
    {
        string clientSecret = "84b3db2eb0360b2a3c028cf8ed5ac89b1964cbff";
        // string accessToken = "405c970d4a4b10a2795664a392cf37499b2a7590";
        string clientId = "2437";

        public async Task<object> CreateStravaAccessToken(string code)
        {
            var newJson = JsonConvert.SerializeObject(new { client_id = clientId, client_secret = clientSecret, code = code });

            var content = new StringContent(newJson, Encoding.UTF8, "application/json");

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("https://www.strava.com/oauth/token"),
                Method = HttpMethod.Post,
                Content = content
            };

            object result = null; ;

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var s = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject(s);
                }
            }

            return result;
        }
    }
}
