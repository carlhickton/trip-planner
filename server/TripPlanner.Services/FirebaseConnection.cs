﻿using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using TripPlanner.Services.Interfaces;

namespace TripPlanner.Services
{

    public class FirebaseConnection : IFirebaseConnection
    {
        const string AuthJson = @"
{
  ""type"": ""service_account"",
  ""project_id"": ""mytripplanner-9edd8"",
  ""private_key_id"": ""51f71877bbf50b3ed957628f2d31d742c8b6f6c3"",
  ""private_key"": ""-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHeuZ2Rsfc5xXG\n6yKyzEywokTaypqNRk1Wg8EaJTINvoWVscbW2YPoh/AcOooKuLzx6O5JOki0F+Fw\nmJtaKPXq4Wi2O9o2wttH/JCJn7C/xB+GzURvmb5yPRI0taLQW9t+c/Qxg7jhoe3J\nHsCw5S4WQk6arVOvxfM1oEyzOV2JaDGzXGzM7B9VAdchWXqIszX9cYixtvOcgCWH\nlczpPhUZcIL6IubtS9V5ccQ10uV/LHndKdcM7MYq8yjD6YVh35UAJsTrZGNToCqN\nGjApzAMoLWzBhoCvz3ezrqz3ZG38o8kywDoVr1hgyUfvmEidDAfqf4qoXEJxucOD\nKb4QYbUJAgMBAAECggEAUER45dD3bUEjNya4kJ+6oDy7Xhq2c/B9ADoGAsJC0NsA\nH8soSp2+AVxp1sILd+L8TI9BCwBD7wvI8CjIIlBnG/tVyjZtSSnBWMcdVfOHCLMf\nSHvidNG1m3xNjh4khJjhypFYCnCJfWlOKGaSfNvLXrORs5XZHdqWUdbN5jRIGhi4\nOA42eq0CQEURrZbj9goZABXPlv4Ikbwp0IXxqkIxT6MChrz+zwCI9jbT5526ND32\nSUMq+Yb5OniLKoy6jVpnlcw621sad+DbWZH79CXEcw64R7j9QvbOx37gkDcwgDZi\nZCVOSwRzYEvpmekWCW7AWI937ggzHRfPYeMmLHj6cQKBgQDmq38kzaOXxCLALszY\nKUfJsF142/dy/LNSCMXVoLY5Y7bn8I9SjNp5yz7ABVE4fEjmZTW8TlleCT2NeSpe\nkMZpnT6kiI0k+jDLDxokB88IDbpl6LR2Uuj6jaCZXB+cjds4VqBmIkkG2RfeZ25e\ns3hOIwKTOAJHeK2IwmC1w37fhQKBgQDdYptt3iV8V1dFSQnx3AkzaZ7iWM4FBOhe\n/0Gj+G80fOI7AJKLl2J6B2obJMsCUAO2gFEt0C0eq3GKSwKDDFgsMKvp1U3LfSL2\nRtaSQIzwdKug8Q7M+2LxdmsJS8316P5074Dy098suJ3fK7OY/AsD51m0JATkPBbk\nwY6GH+i8tQKBgQDRr34hv4mB8brBRwnRSh1G+7rMnfDgJ1T/MphcbP6ZRsrDJwuy\nN/Z2lsqRFYRSu7AtOD3T0sksUWwRyd45HpQu99E1Eih0P7bRMAZMUIjjwk76SsKZ\neR7ufIZMQtE9qDpqiVy/21TVE829z/olvjGTngG+QEDaJZpiYog1UuRnXQKBgB+d\naFLdSX8jPexADPKEGLBF4hGy2uDunXyLeUVAQik9uFTytPrISKDilC2CKJeQ86bJ\nVk5zjy31D8bPBXHPfnv5C2PeOTjZCvgej5/dJgNktenK4vE06SFQZuKx95yphEUQ\nutpylUlep/Tp2cD8JVqOXEoUZrptNlu+pGrU/biNAoGBAK/pnO9vl2XOMBwQaXZR\n21NP+hfWPllZhtZNhlmGvzn4k15I9dYTk+gP9A0pEgt1rZTQuABApe5nGM5PIyDc\njtyWrD653hgz4RM60lffFhMgnTivwfl+Yw63N20IzYstRdx3ZYUcQy86/bVyC1NR\n6H3UVvU8Mwbngz2UXNruxfN8\n-----END PRIVATE KEY-----\n"",
  ""client_email"": ""firebase-adminsdk-7iq0c@mytripplanner-9edd8.iam.gserviceaccount.com"",
  ""client_id"": ""101445395874097063930"",
  ""auth_uri"": ""https://accounts.google.com/o/oauth2/auth"",
  ""token_uri"": ""https://accounts.google.com/o/oauth2/token"",
  ""auth_provider_x509_cert_url"": ""https://www.googleapis.com/oauth2/v1/certs"",
  ""client_x509_cert_url"": ""https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-7iq0c%40mytripplanner-9edd8.iam.gserviceaccount.com""
}";

        public string GetStorageBucket => "mytripplanner-9edd8.appspot.com";

        public IFirebaseClient GetClient()
        {
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = "YjQsWC1V2gz5WWpeySm1Uwn21sChAo1dAHhFtGfg",
                BasePath = "https://mytripplanner-9edd8.firebaseio.com/"
            };

            return new FirebaseClient(config);
        }

        public StorageClient GetStorageClient()
        {
            GoogleCredential credential = GoogleCredential.FromJson(AuthJson);

            // Instantiates a client
            return StorageClient.Create(credential);
        }
    }
}
