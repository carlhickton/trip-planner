﻿using System.Threading.Tasks;
using TripPlanner.Services.Interfaces;
using System.Linq;
using TripPlanner.Services.Models;
using System.Collections.Generic;
using System;
using GeoJSON.Net.Geometry;
using GeoJSON.Net.Feature;
using TripPlanner.Services.Helpers;

namespace TripPlanner.Services
{
    public class StravaService : IStravaService
    {
        private IWebsiteRepository _websiteRepository;
        public StravaService(IWebsiteRepository websiteRepository)
        {
            _websiteRepository = websiteRepository;
        }

        public async Task<bool> AddRouteToDevice(string token, long routeId, string deviceId)
        {
            var auth = new Strava.Authentication.StaticAuthentication(token);

            var client = new Strava.Clients.StravaClient(auth);

            var stravaRoute = client.Routes.GetRoute(routeId);

            var coordinates = GooglePoints.Decode(stravaRoute.Map.Polyline).Select(x => new GeographicPosition(x.Latitude, x.Longitude)).ToList();

            var feature = new Feature(new LineString(coordinates));
            feature.Id = routeId.ToString();
            feature.Properties["name"] = stravaRoute.Name;
            feature.Properties["source"] = "strava";

            await _websiteRepository.AddRouteToDevice(deviceId, feature);

            return true;
        }

        public Task<List<StravaRouteSummary>> Routes(string token, long athleteId)
        {
            var auth = new Strava.Authentication.StaticAuthentication(token);

            var client = new Strava.Clients.StravaClient(auth);

            var result = client.Routes.GetRoutes(athleteId).Select(x => new StravaRouteSummary
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Distance = x.Distance,
                Elevation = x.Elevation,
                SummaryPolyline = x.Map.SummaryPolyline
            }).OrderBy(x => x.Name).ToList();

            return Task.FromResult(result);
        }
    }
}
