﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Google.Cloud.Storage.V1;
using Newtonsoft.Json;
using TripPlanner.Services.Interfaces;
using TripPlanner.Services.Models;
using TripPlanner.Services.Models.Convert;

namespace TripPlanner.Services
{
    public class MapConvertService : IMapConvertService
    {
        private readonly IFirebaseConnection _firebaseConnection;

        public MapConvertService(IFirebaseConnection firebaseConnection)
        {
            _firebaseConnection = firebaseConnection;
        }


        public async Task<MapParseResult> ParseRef(string objectRef)
        {
            MapParseResult result = new MapParseResult();

            var storageClient = _firebaseConnection.GetStorageClient();

            var file = await storageClient.GetObjectAsync(_firebaseConnection.GetStorageBucket, objectRef);

            string value;

            ITrackFileProvider tcx = new TrackFileProviderTcx();
            ITrackFileProvider gpx = new TrackFileProviderGpx();
            ITrackFileProvider kml = new TrackFileProviderKml();

            using (var s = new MemoryStream())
            {
                await storageClient.DownloadObjectAsync(file, s);

                s.Position = 0;

                using (var reader = new StreamReader(s, Encoding.UTF8))
                {
                    value = reader.ReadToEnd();
                }
            }

            List<TrackActivity> item = null;

            var geoJsonPath = string.Empty;

            if (objectRef.EndsWith(".tcx", StringComparison.InvariantCultureIgnoreCase))
            {
                item = tcx.ReadFromString(value).ToList();
                geoJsonPath = objectRef.Replace(".tcx", ".json");
            }
            else if (objectRef.EndsWith(".gpx", StringComparison.InvariantCultureIgnoreCase))
            {
                item = gpx.ReadFromString(value).ToList();
                geoJsonPath = objectRef.Replace(".gpx", ".json");
            }
            else if (objectRef.EndsWith(".kml", StringComparison.InvariantCultureIgnoreCase))
            {
                item = kml.ReadFromString(value).ToList();
                geoJsonPath = objectRef.Replace(".kml", ".json");
            }

            if (item != null && item.Count != 0)
            {
                if (item.Count == 1 && item.First().Laps.Count == 1)
                {
                    var feature = CreateRoute(item.First());

                    using (var stream = GenerateStreamFromString(JsonConvert.SerializeObject(feature)))
                    {
                        var newFile = await storageClient.UploadObjectAsync(_firebaseConnection.GetStorageBucket, geoJsonPath, "application/vnd.geo+json", stream);

                        newFile.CacheControl = "public, max-age=31536000";

                        await storageClient.UpdateObjectAsync(newFile);

                        result.GeoJsonRef = newFile.Name;
                    }

                    result.Distance = item.First().Laps.First().TotalDistance;

                    var points = item.First().Laps.First().Points;

                    if (result.Distance < 1)
                    {
                        result.Distance = GetDistance(points);
                    }

                    result.Elevation = GetElevation(points);

                    result.SummaryMap = CreateMapSummary(points);

                    result.Name = item.First().Name;


                }
            }
            else if (objectRef.EndsWith(".json", StringComparison.InvariantCultureIgnoreCase))
            {

                file.CacheControl = "public, max-age=31536000";
                await storageClient.UpdateObjectAsync(file);

                var feature = JsonConvert.DeserializeObject<Feature>(value);
                var line = feature.Geometry as LineString;
                if (line == null)
                {
                    return null;
                }

                var points = line.Coordinates.OfType<GeographicPosition>()
                    .Select(x => new TrackPoint
                    {
                        Longitude = Convert.ToSingle(x.Longitude),
                        Latitude = Convert.ToSingle(x.Latitude),
                        Altitude = Convert.ToSingle(x.Altitude.GetValueOrDefault(0))
                    }).ToList();

                if (!string.IsNullOrEmpty(feature.Properties["distance"]?.ToString()))
                {
                    result.Distance = double.Parse(feature.Properties["distance"].ToString());
                }

                if (!string.IsNullOrEmpty(feature.Properties["elevation"]?.ToString()))
                {
                    result.Elevation = double.Parse(feature.Properties["elevation"].ToString());
                }

                if (result.Distance < 1)
                {
                    result.Distance = GetDistance(points);
                }

                if (result.Elevation < 1)
                {
                    result.Elevation = GetElevation(points);
                }

                result.SummaryMap = CreateMapSummary(points);
            }

            return result;
        }

        private double GetDistance(List<TrackPoint> points)
        {
            double totalDistance = 0;

            GeoCoordinate previous = null;

            foreach (var trackPoint in points)
            {
                var current = new GeoCoordinate(trackPoint.Latitude, trackPoint.Longitude);

                if (previous != null)
                {
                    var distance = previous.GetDistanceTo(current);
                    totalDistance = totalDistance + distance;
                }

                previous = current;
            }


            return totalDistance;
        }

        private double GetElevation(List<TrackPoint> points)
        {
            double? totalElevationGain = null;

            double previousAltitude = 0;

            foreach (var trackPoint in points)
            {
                if (totalElevationGain != null)
                {
                    if (previousAltitude < trackPoint.Altitude)
                    {
                        totalElevationGain = totalElevationGain + (trackPoint.Altitude - previousAltitude);
                    }
                }
                else
                {
                    totalElevationGain = trackPoint.Altitude;
                }

                previousAltitude = trackPoint.Altitude;
            }


            return totalElevationGain.GetValueOrDefault(0);
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private string CreateMapSummary(List<TrackPoint> points)
        {

            var coordinates = points.Select(x => new EncodeCoordinateEntity(Convert.ToSingle(x.Latitude), Convert.ToSingle(x.Longitude))).ToList();

            var result = Helpers.GooglePoints.Encode(coordinates);

            while (result.Length > 7000)
            {
                coordinates = ReducePoints(coordinates);
                result = Helpers.GooglePoints.Encode(coordinates);
            }

            return result;

        }

        private List<EncodeCoordinateEntity> ReducePoints(List<EncodeCoordinateEntity> coordinates)
        {
            var result = new List<EncodeCoordinateEntity>();
            var counter = 0;

            foreach (var coord in coordinates)
            {
                if ((counter % 2) == 0)
                {
                    result.Add(coord);
                }
                counter++;
            }

            return result;
        }

        private Feature CreateRoute(TrackActivity item)
        {
            var points = item.Laps.First().Points;

            var coordinates = points.Select(x => new GeographicPosition(x.Latitude, x.Longitude, x.Altitude)).ToList();

            var distance = item.TotalDistance;

            if (distance < 1)
            {
                distance = GetDistance(points);
            }


            var feature = new Feature(new LineString(coordinates));
            feature.Properties.Add("name", item.Name);
            feature.Properties.Add("description", item.Description);
            feature.Properties.Add("distance", distance);
            feature.Properties.Add("elevation", coordinates.Max(x => x.Altitude.GetValueOrDefault(0)) - coordinates.Min(x => x.Altitude.GetValueOrDefault(0)));

            feature.Id = Guid.NewGuid().ToString();

            return feature;
        }


    }

    public interface IMapOutGenerator
    {
        Task<string> ParseRef(string objectRef);
    }

    public interface IMapConvertService
    {
        Task<MapParseResult> ParseRef(string objectRef);
    }

    public class MapParseResult
    {
        public string GeoJsonRef { get; set; }

        public string SummaryMap { get; set; }

        public double Distance { get; set; }

        public double Elevation { get; set; }

        public string Name { get; set; }
    }
}