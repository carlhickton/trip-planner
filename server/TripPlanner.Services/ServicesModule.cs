﻿using Autofac;

namespace TripPlanner.Services
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<FirebaseConnection>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WebsiteRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AuthService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StravaService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<GarminService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<MapConvertService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RoutingService>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
