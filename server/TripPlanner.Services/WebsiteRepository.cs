﻿using GeoJSON.Net.Feature;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TripPlanner.Services.Interfaces;
using TripPlanner.Services.Models;

namespace TripPlanner.Services
{
    public class WebsiteRepository : IWebsiteRepository
    {
        IFirebaseConnection _firebaseConnection;

        public WebsiteRepository(IFirebaseConnection firebaseConnection)
        {
            _firebaseConnection = firebaseConnection;
        }

        public async Task<bool> AddRouteToDevice(string deviceId, Feature feature)
        {
            var response = await _firebaseConnection.GetClient().PushAsync("devices/" + deviceId + "/routes", feature);
            return true;
        }

        public async Task<Device> GetDevice(string device)
        {
            var deviceResponse = await _firebaseConnection.GetClient().GetAsync("devices/" + device);

            // var foo = await GetDeviceRoutes(device);

            return deviceResponse.ResultAs<Device>();
        }

        public async Task<bool> RemoveDeviceRoutes(string device, string routeKey)
        {
            await _firebaseConnection.GetClient()
                .DeleteAsync("devices/" + device + "/routes/" + routeKey);
            return true;
        }

        private class FireBaseEntity<T>
        {
            public string Id { get; set; }
        }

        public async Task<Device> UpdateDeviceSync(string device)
        {
            var patch = new
            {
                syncDate = DateTimeOffset.UtcNow
            };

            var response = await _firebaseConnection.GetClient().UpdateAsync("devices/" + device, patch);
            return response.ResultAs<Device>();
        }
    }
}
