﻿using System.Threading.Tasks;

namespace TripPlanner.Services.Interfaces
{
    public interface IAuthService
    {
        Task<object> CreateStravaAccessToken(string code);
    }
}
