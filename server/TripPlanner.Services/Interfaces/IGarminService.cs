using System.Collections.Generic;
using System.IO;
using GeoJSON.Net.Feature;

namespace TripPlanner.Services.Interfaces
{
    public interface IGarminService
    {
        Stream CreateFitFile(Feature feature);
        string CreateDeviceKey();
    }
}