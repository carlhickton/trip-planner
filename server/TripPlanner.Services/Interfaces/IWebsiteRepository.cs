﻿using GeoJSON.Net.Feature;
using System.Threading.Tasks;
using TripPlanner.Services.Models;

namespace TripPlanner.Services.Interfaces
{
    public interface IWebsiteRepository
    {
        Task<Device> GetDevice(string device);
        Task<Device> UpdateDeviceSync(string device);
        Task<bool> AddRouteToDevice(string deviceId, Feature feature);

        Task<bool> RemoveDeviceRoutes(string device, string routeKey);
    }
}
