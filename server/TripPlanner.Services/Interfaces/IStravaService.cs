﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TripPlanner.Services.Models;

namespace TripPlanner.Services.Interfaces
{
    public interface IStravaService
    {
        Task<List<StravaRouteSummary>> Routes(string token, long athleteId);

        Task<bool> AddRouteToDevice(string token, long routeId, string deviceId);
    }
}
