﻿using FireSharp.Interfaces;
using Google.Cloud.Storage.V1;

namespace TripPlanner.Services.Interfaces
{
    public interface IFirebaseConnection
    {
        string GetStorageBucket { get; }
        IFirebaseClient GetClient();

        StorageClient GetStorageClient();
    }
}
