// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  googleMapApi: 'AIzaSyBmDVdNK4QzhE0-FQdVleHbaBJJDYey6vw',
  // googleMapApi: 'AIzaSyCLTBTK7SYP46gy6ju8Dkk0eAcNUzMLoOo',
  hostUrl: 'https://localhost:4200/',
  production: false,
  // serverUrl: 'https://localhost:44360/',
  serverUrl: 'https://trip-planner.azurewebsites.net/',
};
