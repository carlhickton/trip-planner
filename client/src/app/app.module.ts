import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { MdButtonModule, MdIconModule, MdListModule, MdSidenavModule, MdToolbarModule } from '@angular/material';
import { AngularFireModule } from 'angularfire2';

import 'hammerjs';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TripCommonModule } from './common/common.module';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { StravaAuthPageComponent } from './pages/strava-auth-page/strava-auth-page.component';

// Must export the config
export const firebaseConfig = {
  apiKey: 'AIzaSyBmDVdNK4QzhE0-FQdVleHbaBJJDYey6vw',
  authDomain: 'mytripplanner-9edd8.firebaseapp.com',
  databaseURL: 'https://mytripplanner-9edd8.firebaseio.com',
  messagingSenderId: '682269837125',
  projectId: 'mytripplanner-9edd8',
  storageBucket: 'mytripplanner-9edd8.appspot.com',
};

const routes: Routes = [
  {
    component: HomePageComponent,
    data: { title: '' },
    path: '',
  },
  {
    component: LoginPageComponent,
    data: { title: 'Login' },
    path: 'login',
  },
  {
    component: StravaAuthPageComponent,
    data: { title: 'Loading Strava Profile' },
    path: 'auth/strava',
  },
  {
    data: { title: 'Maps', type: 'map' },
    loadChildren: './map/map.module#MapModule',
    path: 'map',
  },
  {
    loadChildren: './user/user.module#UserModule',
    path: 'settings',
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '',
  },
];

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginPageComponent,
    StravaAuthPageComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    // MaterialModule.forRoot(),
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdButtonModule,
    MdListModule,

    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes),
    TripCommonModule.forRoot(),
  ],
  providers: [],
})

export class AppModule { }
