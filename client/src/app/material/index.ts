import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MdButtonModule } from '@angular/material';

import {
    MdFabSpeedDialActionsComponent,
    MdFabSpeedDialComponent,
    MdFabSpeedDialTriggerComponent,
} from './fab-speed-dial.component';

@NgModule({
    declarations: [
        MdFabSpeedDialTriggerComponent,
        MdFabSpeedDialActionsComponent,
        MdFabSpeedDialComponent,
    ],
    exports: [
        MdFabSpeedDialTriggerComponent,
        MdFabSpeedDialActionsComponent,
        MdFabSpeedDialComponent,
    ],
    imports: [
        CommonModule,
        MdButtonModule,
    ],
})
export class MdFabSpeedDialModule {

}
