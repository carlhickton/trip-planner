import { ContentChildren, forwardRef, Inject, QueryList } from '@angular/core';
import {
    AfterContentInit,
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    Output,
    Renderer,
    ViewEncapsulation,
} from '@angular/core';
import { MdButton } from '@angular/material';

const Z_INDEX_ITEM: number = 23;

@Component({
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '[class.mat-fab-actions]': 'true',
    },
    selector: 'md-fab-actions, mat-fab-actions',
    template: `
        <ng-content select="[md-mini-fab], [mat-mini-fab]"></ng-content>
    `,
})
export class MdFabSpeedDialActionsComponent implements AfterContentInit {

    @ContentChildren(MdButton) private buttons: QueryList<MdButton>;

    // tslint:disable-next-line:no-forward-ref
    constructor(@Inject(forwardRef(() => MdFabSpeedDialComponent)) private parent: MdFabSpeedDialComponent,
                private renderer: Renderer) {
    }

    public ngAfterContentInit(): void {
        this.buttons.changes.subscribe(() => {
            this.initButtonStates();
            this.parent.setActionsVisibility();
        });

        this.initButtonStates();
    }

    public show() {
        if (this.buttons) {
            this.buttons.toArray().forEach((button, i) => {
                let delay = 0;
                let transform: string;
                if (this.parent.animationMode === 'scale') {
                    // Incremental transition delay of 65ms for each action button
                    delay = 3 + (65 * i);
                    transform = 'scale(1)';
                } else {
                    transform = this.getTranslateFunction('0');
                }
                this.changeElementStyle(button._getHostElement(), 'transition-delay', delay + 'ms');
                this.changeElementStyle(button._getHostElement(), 'opacity', '1');
                this.changeElementStyle(button._getHostElement(), 'transform', transform);
            });
        }
    }

    public hide() {
        if (this.buttons) {
            this.buttons.toArray().forEach((button, i) => {
                let opacity = '1';
                let delay = 0;
                let transform: string;
                if (this.parent.animationMode === 'scale') {
                    delay = 3 - (65 * i);
                    transform = 'scale(0)';
                    opacity = '0';
                } else {
                    transform = this.getTranslateFunction((55 * (i + 1) - (i * 5)) + 'px');
                }
                this.changeElementStyle(button._getHostElement(), 'transition-delay', delay + 'ms');
                this.changeElementStyle(button._getHostElement(), 'opacity', opacity);
                this.changeElementStyle(button._getHostElement(), 'transform', transform);
            });
        }
    }

    private initButtonStates() {
        this.buttons.toArray().forEach((button, i) => {
            this.renderer.setElementClass(button._getHostElement(), 'mat-fab-action-item', true);
            this.changeElementStyle(button._getHostElement(), 'z-index', '' + (Z_INDEX_ITEM - i));
        });
    }

    private getTranslateFunction(value: string) {
        const dir = this.parent.direction;
        const translateFn = (dir === 'up' || dir === 'down') ? 'translateY' : 'translateX';
        const sign = (dir === 'down' || dir === 'right') ? '-' : '';
        return translateFn + '(' + sign + value + ')';
    }

    private changeElementStyle(elem: any, style: string, value: string) {
        // FIXME - Find a way to create a "wrapper" around the action button(s),
        // so we don't change it's style tag
        this.renderer.setElementStyle(elem, style, value);
    }
}

// tslint:disable-next-line:max-classes-per-file
@Component({
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '[class.mat-fab-trigger]': 'true',
        '[class.mat-spin]': 'spin',
    },
    selector: 'md-fab-trigger, mat-fab-trigger',
    template: `
        <ng-content select="[md-fab], [mat-fab]"></ng-content>
    `,
})
export class MdFabSpeedDialTriggerComponent {

    /**
     * Whether this trigger should spin (360dg) while opening the speed dial
     */
    @Input() public spin: boolean = false;

    // tslint:disable-next-line:no-forward-ref
    constructor(@Inject(forwardRef(() => MdFabSpeedDialComponent)) private parent: MdFabSpeedDialComponent) {
    }

    @HostListener('click', ['$event'])
    public _onClick(event: any) {
        if (!this.parent.fixed) {
            this.parent.toggle();
            event.stopPropagation();
        }
    }

}

// tslint:disable-next-line:max-classes-per-file
@Component({
    encapsulation: ViewEncapsulation.None,
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '[class.mat-fab-speed-dial]': 'true',
        '[class.mat-opened]': 'open',
    },
    selector: 'md-fab-speed-dial, mat-fab-speed-dial',
    styleUrls: ['./fab-speed-dial.scss'],
    template: `
        <div class="mat-fab-speed-dial-container">
            <ng-content select="md-fab-trigger, mat-fab-trigger"></ng-content>
            <ng-content select="md-fab-actions, mat-fab-actions"></ng-content>
        </div>
    `,
})
export class MdFabSpeedDialComponent implements AfterContentInit {
    /**
     * Whether this speed dial is fixed on screen (user cannot change it by clicking)
     */
    @Input() public fixed: boolean = false;

    /**
     * Whether this speed dial is opened
     */
    @Input() public get open() {
        return this._open;
    }

    public set open(open: boolean) {
        open = open === true;
        const previousOpen = this._open;
        this._open = open;
        if (previousOpen !== this._open) {
            this.openChange.emit(this._open);
            if (this.isInitialized) {
                this.setActionsVisibility();
            }
        }
    }

    /**
     * The direction of the speed dial. Can be 'up', 'down', 'left' or 'right'
     */
    @Input() public get direction() {
        return this._direction;
    }

    public set direction(direction: string) {
        direction = direction ? direction : 'up';
        const previousDir = this._direction;
        this._direction = direction;
        if (previousDir !== this.direction) {
            this._setElementClass(previousDir, false);
            this._setElementClass(this.direction, true);

            if (this.isInitialized) {
                this.setActionsVisibility();
            }
        }
    }

    /**
     * The animation mode to open the speed dial. Can be 'fling' or 'scale'
     */
    @Input() public get animationMode() {
        return this._animationMode;
    }

    public set animationMode(animationMode: string) {
        animationMode = animationMode ? animationMode : 'fling';
        const previousAnimationMode = this._animationMode;
        this._animationMode = animationMode;
        if (previousAnimationMode !== this._animationMode) {
            this._setElementClass(previousAnimationMode, false);
            this._setElementClass(this.animationMode, true);

            if (this.isInitialized) {
                // To start another detect lifecycle and force the close on the action buttons
                Promise.resolve(null).then(() => this.open = false);
            }
        }
    }

    @Output() public openChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    // tslint:disable-next-line:variable-name
    @ContentChild(MdFabSpeedDialActionsComponent) private _childActions: MdFabSpeedDialActionsComponent;

    private isInitialized: boolean = false;
    // tslint:disable-next-line:variable-name
    private _direction: string = 'up';
    // tslint:disable-next-line:variable-name
    private _open: boolean = false;
    // tslint:disable-next-line:variable-name
    private _animationMode: string = 'fling';

    constructor(private elementRef: ElementRef, private renderer: Renderer) {
        this._childActions = new MdFabSpeedDialActionsComponent(this, renderer);
        this._open = false;
    }

    public ngAfterContentInit(): void {
        this.isInitialized = true;
        this.setActionsVisibility();
        this._setElementClass(this.direction, true);
        this._setElementClass(this.animationMode, true);
    }

    /**
     * Toggle the open state of this speed dial
     */
    public toggle() {
        this.open = !this.open;
    }

    public setActionsVisibility() {
        if (this.open) {
            this._childActions.show();
        } else {
            this._childActions.hide();
        }
    }

    @HostListener('click')
    public onClick() {
        if (!this.fixed && this.open) {
            this.open = false;
        }
    }

    private _setElementClass(elemClass: string, isAdd: boolean) {
        this.renderer.setElementClass(this.elementRef.nativeElement, `mat-${elemClass}`, isAdd);
    }
}
