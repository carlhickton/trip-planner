import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdProgressBarModule,
    MdSelectModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from '../common/services/auth-guard.service';
import { ManualRoutesComponent } from './pages/manual-routes/manual-routes.component';
import { DeviceDialogComponent } from './pages/settings/device-dialog/device-dialog.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { StravaRoutesComponent } from './pages/strava-routes/strava-routes.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {
    MoveRouteDialogComponent,
 } from './pages/manual-routes/route-card/move-route-dialog/move-route-dialog.component';
import { RouteCardComponent } from './pages/manual-routes/route-card/route-card.component';
import { GroupDialogComponent } from './pages/settings/group-dialog/group-dialog.component';

@NgModule({
    bootstrap: [DeviceDialogComponent],
    declarations: [
        StravaRoutesComponent,
        SettingsComponent,
        DeviceDialogComponent,
        GroupDialogComponent,
        MoveRouteDialogComponent,
        ManualRoutesComponent,
        RouteCardComponent,
    ],
    entryComponents: [
        DeviceDialogComponent,
        GroupDialogComponent,
        MoveRouteDialogComponent,
    ],
    exports: [],
    imports: [
        CommonModule,

        AngularFireModule,
        AngularFireAuthModule,
        AngularFireDatabaseModule,

        MdDialogModule,
        MdButtonModule,
        MdInputModule,
        MdSelectModule,
        MdIconModule,
        MdListModule,
        MdCardModule,
        MdProgressBarModule,
        MdMenuModule,

        ReactiveFormsModule,
        RouterModule.forChild([
            {
                canActivate: [AuthGuardService],
                component: SettingsComponent,
                data: { title: 'User Settings' },
                path: '',
            },
            {
                canActivate: [AuthGuardService],
                component: StravaRoutesComponent,
                data: { title: 'Strava Routes' },
                path: 'routes/strava',
            },
            {
                canActivate: [AuthGuardService],
                component: ManualRoutesComponent,
                data: { title: 'Manual Routes' },
                path: 'routes/:group',
            },
        ]),
    ],
    providers: [
    ],
})
export class UserModule {
}
