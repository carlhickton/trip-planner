import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { DeviceService } from 'app/common/services/device.service';
import { NotifyService } from 'app/common/services/notify.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../../environments/environment';
import { AuthService, IUserProfile } from '../../../common/services/auth.service';
import { IListDevice } from '../../../common/services/device.service';
import { IRouteSummary, StravaService } from '../../../common/services/strava.service';

@Component({
  selector: 'strava-routes',
  styleUrls: ['./strava-routes.component.scss'],
  templateUrl: './strava-routes.component.html',
})
export class StravaRoutesComponent implements OnInit, OnDestroy {
  public isProcessing: boolean;
  public profile: IUserProfile;
  public code: string;
  public routes: Observable<IRouteSummary[]>;
  public devices: Observable<IListDevice[]>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private authService: AuthService,
    private stravaService: StravaService,
    private sanitizer: DomSanitizer,
    private notifyService: NotifyService,
    private deviceService: DeviceService) {
    this.isProcessing = false;
  }

  public ngOnInit() {
    this.authService.profile()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.profile = x;
        this.routes = this.stravaService
          .getRoutes(this.profile.strava_access_token, this.profile.strava_athlete_id);
      });

    this.devices = this.deviceService.listDevices();
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public getImage(route: IRouteSummary): string {
    let result = 'https://maps.googleapis.com/maps/api/staticmap';
    // result += '?center=40.714728,-73.998672&zoom=14&size=400x400';
    result += '?size=320x200';
    result += '&path=weight:5|color:blue|enc:' + route.summaryPolyline;
    result += '&key=' + environment.googleMapApi;
    return result;
  }

  public getBackGroundImage(route: IRouteSummary): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + this.getImage(route) + ')');
  }

  public trackByRoute(route: IRouteSummary) {
    return route.id;
  }

  public sendToDevice(device: IListDevice, route: IRouteSummary) {
    this.isProcessing = true;
    this.notifyService.showMessage('Quing Route');
    this.stravaService.sendRoute(this.profile.strava_access_token, route.id, device.deviceId).subscribe(() => {
      this.isProcessing = false;
      this.notifyService.showMessage('Route Sent to ' + device.deviceId + ' Queue');
    }, () => {
      this.notifyService.showMessage('Error Sending to Device Queue');
      this.isProcessing = false;
    });
  }
}
