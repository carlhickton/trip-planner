import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { DeviceService, IListDevice } from 'app/common/services/device.service';
import { GroupService } from 'app/common/services/group.service';
import { IManualRoute, RoutesService } from 'app/common/services/routes.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../../environments/environment';
import { NotifyService } from '../../../common/services/notify.service';

import { ActivatedRoute, Params } from '@angular/router';
// import * as _ from 'lodash';
import { IMoveRouteOptions } from './route-card/route-card.component';

@Component({
  selector: 'manual-routes',
  styleUrls: ['./manual-routes.component.scss'],
  templateUrl: './manual-routes.component.html',
})
export class ManualRoutesComponent implements OnInit, OnDestroy {

  // public routes: Observable<IManualRoute[]>;
  public routes: string[];
  public groupRoutes: Observable<IManualRoute[]>;
  // public groupRoutes: IManualRoute[];
  public devices: Observable<IListDevice[]>;

  public groupName: string;

  @ViewChild('fileInput') public fileInput: ElementRef;

  public isProcessing = false;
  public mode = 'indeterminate';

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private routesService: RoutesService,
    private notifyService: NotifyService,
    private deviceService: DeviceService,
    private groupService: GroupService,
    private route: ActivatedRoute) { }

  public ngOnInit() {
    console.log('Manual ROutes');

    this.isProcessing = true;

    this.route.params
      .takeUntil(this.ngUnsubscribe)
      .subscribe((params: Params) => {
        console.log('group', params);
        this.groupName = params.group;

        this.groupService.getGroupRoutes(this.groupName)
          .takeUntil(this.ngUnsubscribe)
          .subscribe((x) => {
            if ((x as any).$value !== null && (x as any).$value !== true) {
              this.routes = x.values as string[];
            }

            this.isProcessing = false;
          });
      });

    this.devices = this.deviceService.listDevices();

  }

  public sendToDevice(device: IListDevice, route: IManualRoute) {
    this.routesService.sendToDevice(device.deviceId, route);
    console.log(device, route);
  }

  public openFileDialog() {
    this.fileInput.nativeElement.click();
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    console.log('ngOnDestroy ManualRoutesComponent');
  }

  public getImage(route: IManualRoute): string {
    let result = 'https://maps.googleapis.com/maps/api/staticmap';
    // result += '?center=40.714728,-73.998672&zoom=14&size=400x400';
    result += '?size=320x200';
    result += '&path=weight:5|color:blue|enc:' + route.summary;
    result += '&key=' + environment.googleMapApi;
    return result;
  }

  public moveRoute(event: IMoveRouteOptions) {
    console.log(event);
    this.mode = 'indeterminate';
    this.isProcessing = true;
    this.groupService.addGroupPath(event.groupName, event.route.$key)
      .take(1)
      .subscribe(() => {
         this.mode = 'query';
         this.groupService.removeGroupPath(this.groupName, event.route.$key)
          .take(1)
          .subscribe(() => {
            this.isProcessing = false;
            this.notifyService.showMessage('Route Moved to ' + event.groupName);
          });
      });
  }

  public deleteRoute(route: IManualRoute) {
    this.mode = 'indeterminate';
    this.isProcessing = true;
    this.routesService.deleteRoute(route).subscribe(() => {
      console.log('FInished Remoing Route');
      this.isProcessing = false;
      this.notifyService.showMessage('Deleted Route');
      this.groupService.removeGroupPath(this.groupName, route.$key)
        .take(1)
        .subscribe((x) => {
          console.log(x);
        });
    }, (err) => {
      console.log('error');
      this.isProcessing = false;
      this.notifyService.showMessage('Error: ' + err.code);
      console.log(err);
    });
  }

  public fileUpload($event: Event) {

    const inputElement = ($event.target as HTMLInputElement);

    this.routesService.saveFiles(inputElement.files, this.groupName).subscribe((state) => {
      if (!state.isParsing) {
        this.isProcessing = true;
        this.mode = 'indeterminate';
      }
      if (state.isParsing && !state.isCompleate) {
        this.mode = 'query';
      } else if (state.isCompleate) {
        this.isProcessing = false;
        this.notifyService.showMessage('File Saved');
        inputElement.value = '';
      }
    }, (err) => {
      this.isProcessing = false;
      this.notifyService.showMessage('Failed to Save File');
      inputElement.value = '';
    });
  }

  public addToGroup(groupName: string, route: IManualRoute) {
    this.groupService.addGroupPath(groupName, route.$key);
  }
}
