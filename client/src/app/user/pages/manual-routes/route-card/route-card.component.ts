import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MdDialog } from '@angular/material';

import { FirebaseObjectObservable } from 'angularfire2/database';
import { ConfirmDialogComponent } from 'app/common/confirm-dialog/confirm-dialog.component';
import { IListDevice } from 'app/common/services/device.service';
import { GroupService } from 'app/common/services/group.service';
import { IManualRoute, RoutesService } from 'app/common/services/routes.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { MoveRouteDialogComponent } from './move-route-dialog/move-route-dialog.component';

export interface IMoveRouteOptions {
  route: IManualRoute;
  groupName: string;
}

@Component({
  selector: 'route-card',
  styleUrls: ['./route-card.component.scss'],
  templateUrl: './route-card.component.html',
})
export class RouteCardComponent implements OnInit, OnDestroy {

  @Input()
  public route: FirebaseObjectObservable<IManualRoute>;

  @Input()
  public devices: Observable<IListDevice[]>;

  @Input()
  public routeKey: string;

  @Output()
  public onDelete = new EventEmitter<IManualRoute>();

  @Output()
  public onMove = new EventEmitter<IMoveRouteOptions>();

  public r: IManualRoute;

  public isProcessing = false;
  public mode = 'indeterminate';

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private routesService: RoutesService,
    // private notifyService: NotifyService,
    private groupService: GroupService,
    private dialog: MdDialog) { }

  public ngOnInit() {

    if (this.route) {
      this.route.takeUntil(this.ngUnsubscribe).subscribe((x) => {
        this.r = x;
      });
    }

    if (this.routeKey) {
      this.routesService.getRouteByKey(this.routeKey)
        .takeUntil(this.ngUnsubscribe)
        .subscribe((x) => {
          console.log(this.routeKey, x);
          this.r = x;
        });
    }
  }

  public sendToDevice(device: IListDevice, route: IManualRoute) {
    this.routesService.sendToDevice(device.deviceId, route);
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public getImage(route: IManualRoute): string {
    let result = 'https://maps.googleapis.com/maps/api/staticmap';
    // result += '?center=40.714728,-73.998672&zoom=14&size=400x400';
    result += '?size=320x200';
    result += '&path=weight:5|color:blue|enc:' + route.summary;
    result += '&key=' + environment.googleMapApi;
    return result;
  }

  public confirmDelete(route: IManualRoute) {
    const dialog = this.dialog.open(ConfirmDialogComponent);
    dialog.componentInstance.message = 'Are you sure';

    dialog.afterClosed().take(1).subscribe((x: boolean) => {
      if (x) {
        this.onDelete.emit(route);
      }
    });
  }

  public showMoveOptions(route: IManualRoute) {
    const dialog = this.dialog.open(MoveRouteDialogComponent);

    dialog.afterClosed().take(1).subscribe((x: string) => {
      if (x) {
        this.onMove.emit({
          route,
          groupName: x,
        });
        // console.log(x);
        // this.onDelete.emit(route);
      }
    });
  }

  public addToGroup(groupName: string, route: IManualRoute) {
    this.groupService.addGroupPath(groupName, route.$key);
  }
}
