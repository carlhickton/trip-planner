import { Component, OnDestroy, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { GroupService } from '../../../../../common/services/group.service';

@Component({
  selector: 'move-route-dialog',
  templateUrl: './move-route-dialog.component.html',
})
export class MoveRouteDialogComponent implements OnInit, OnDestroy {

  public groups: Observable<string[]>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<MoveRouteDialogComponent>,
    groupService: GroupService) {

      this.groups = groupService.listGroups();
  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px', '400px');
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public moveRoute(groupName: string) {
    this.dialogRef.close(groupName);
  }

  public close() {
    this.dialogRef.close(null);
  }
}
