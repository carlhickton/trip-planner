import { Component, OnDestroy, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';

import { DeviceService } from 'app/common/services/device.service';
import { GroupService } from 'app/common/services/group.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AuthService, IUserProfile } from '../../../common/services/auth.service';
import { IListDevice } from '../../../common/services/device.service';
import { DeviceDialogComponent } from './device-dialog/device-dialog.component';
import { GroupDialogComponent } from './group-dialog/group-dialog.component';

@Component({
  selector: 'settings',
  styleUrls: ['./settings.component.scss'],
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit, OnDestroy {

  public profile: IUserProfile;
  public devices: Observable<IListDevice[]>; // FirebaseListObservable<IProfileDevice[]>;
  public groups : any[];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private authService: AuthService,
    private dialog: MdDialog,
    private deviceService: DeviceService,
    private groupService: GroupService) { }

  public ngOnInit() {
    this.authService.profile()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.profile = x;
      });

    this.groupService.listGroups()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.groups = x;
      });

    this.devices = this.deviceService.listDevices();
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public trackByDevice(device: IListDevice) {
    return device.deviceId;
  }

  public openDialog() {
    this.dialog.open(DeviceDialogComponent);
  }

  public createGroup() {
    this.dialog.open(GroupDialogComponent);
  }
}
