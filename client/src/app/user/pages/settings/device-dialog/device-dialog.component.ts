import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { DeviceService } from 'app/common/services/device.service';
import { Subject } from 'rxjs/Subject';

interface IDeviceForm {
  name: string;
  deviceId: string;
  deviceType: string;
}

@Component({
  selector: 'device-dialog',
  templateUrl: './device-dialog.component.html',
})
export class DeviceDialogComponent implements OnInit, OnDestroy {
  public deviceForm: FormGroup;

  public devices = [
    { value: 'edge520', viewValue: 'Edge 520' },
    { value: 'edge820', viewValue: 'Edge 820' },
    { value: 'edge1000', viewValue: 'Edge 1000' },
  ];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<DeviceDialogComponent>,
    private deviceService: DeviceService) {
    this.deviceForm = new FormGroup({
      deviceId: new FormControl(),
      deviceType: new FormControl(),
      name: new FormControl(),
    });

  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px');
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public saveDevice() {
    if (!this.deviceForm.invalid) {

      const value: IDeviceForm = this.deviceForm.value;

      this.deviceService.newDevice(value.name, value.deviceId, value.deviceType)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(() => {
          this.dialogRef.close();
        });

    }
  }

}
