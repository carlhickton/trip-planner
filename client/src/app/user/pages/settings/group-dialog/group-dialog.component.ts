import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { Subject } from 'rxjs/Subject';
import { GroupService } from '../../../../common/services/group.service';

interface IGroupForm {
  name: string;
}

@Component({
  selector: 'group-dialog',
  templateUrl: './group-dialog.component.html',
})
export class GroupDialogComponent implements OnInit, OnDestroy {
  public groupForm: FormGroup;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<GroupDialogComponent>,
    private groupService: GroupService) {

    this.groupForm = new FormGroup({
      name: new FormControl(),
    });

  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px');
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public saveGroup() {
    if (!this.groupForm.invalid) {
      const value: IGroupForm = this.groupForm.value;

      this.groupService.saveGroup(value.name)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(() => {
          this.dialogRef.close();
        }, (err) => console.log(err));
    }
  }

}
