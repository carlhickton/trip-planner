import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import { AuthService } from '../../common/services/auth.service';

@Component({
  selector: 'app-login-page',
  styleUrls: ['./login-page.component.css'],
  templateUrl: './login-page.component.html',
})
export class LoginPageComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(public authService: AuthService, private router: Router) {

  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public ngOnInit(): void {
    this.authService.auth()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((auth) => {
        if (auth != null) {
          this.router.navigate([this.authService.redirectUrl]);
        }
      });
  }

  public login() {
    this.authService.loginWithGoogle().then(() => {
      this.router.navigate([this.authService.redirectUrl]);
    });
  }
}
