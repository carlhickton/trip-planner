import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import { AuthService } from '../../common/services/auth.service';

@Component({
  selector: 'strava-auth-page',
  templateUrl: './strava-auth-page.component.html',
})
export class StravaAuthPageComponent implements OnInit, OnDestroy {

  public code: string;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private route: ActivatedRoute, private authService: AuthService, private router: Router) { }

  public ngOnInit() {

    this.route.queryParams
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.code = x.code;

        this.authService.addToken(this.code)
          .takeUntil(this.ngUnsubscribe)
          .subscribe(() => {
            this.router.navigate(['settings/routes/strava']);
          });
      });
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
