import { Component, OnDestroy, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
})
export class ConfirmDialogComponent implements OnInit, OnDestroy {

  public message: string;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<ConfirmDialogComponent>) {
  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px');
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public ok() {
    this.dialogRef.close(true);
  }

  public cancel() {
    this.dialogRef.close(false);
  }

}
