import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MdSnackBarModule, MdDialogModule, MdButtonModule } from '@angular/material';

import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { DeviceService } from './services/device.service';
import { MapService } from './services/map.service';
import { NotifyService } from './services/notify.service';
import { QueryService } from './services/query.service';
import { RoutesService } from './services/routes.service';
import { StravaService } from './services/strava.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { GroupService } from './services/group.service';

@NgModule({
    declarations: [
        ConfirmDialogComponent,
    ],
    entryComponents: [
        ConfirmDialogComponent,
    ],
    exports: [],
    imports: [
        HttpModule,
        MdSnackBarModule,
        MdDialogModule,
        MdButtonModule,

        AngularFireModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
    ],
    providers: [],
})
export class TripCommonModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: TripCommonModule,
            providers: [
                MapService,
                AuthService,
                QueryService,
                StravaService,
                DeviceService,
                AuthGuardService,
                NotifyService,
                RoutesService,
                GroupService,
            ],
        };
    }
}
