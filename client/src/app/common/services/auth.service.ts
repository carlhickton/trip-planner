import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// import { AngularFire, AngularFireAuth, AuthMethods, AuthProviders, FirebaseApp } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../environments/environment';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

// import 'firebase';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs/Subscription';
// declare const firebase: any;

export interface IUserProfile {
  username: string;
  email: string;
  icon: string;
  strava_access_token: string;
  strava_athlete_id: number;
}

export interface IStravaToken {
  access_token: string;
  athlete: IStravaAthlete;
  token_type: string;
}

export interface IStravaAthlete {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  profile: string;
  profile_medium: string;
}

@Injectable()
export class AuthService {

  public isLoggedIn: boolean = false;
  public userId: string = null;

  public redirectUrl: string;

  private profileSubject = new ReplaySubject<IUserProfile>();

  private profileSub: Subscription;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private angularFireDatabase: AngularFireDatabase,
    private http: Http) {

    this.redirectUrl = '';
    this.profileSubject = new ReplaySubject(1);

    this.angularFireAuth.authState.subscribe((authState) => {
      console.log('AuthService', authState);
      if (authState === null) {
        this.isLoggedIn = false;
        this.userId = null;
        this.profileSubject.next(null);
        if (this.profileSub) {
          this.profileSub.unsubscribe();
        }
      } else {
        this.isLoggedIn = true;
        this.userId = authState.uid;
        this.profileSub = this.angularFireDatabase.object('users/' + this.userId + '/profile')
          .subscribe((x) => {
            this.profileSubject.next(x);
          }, (err) => {
            console.log(err);
            this.profileSubject.next(null);
          });
      }
    });
  }

  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  public loginWithGoogle() {
    return this.angularFireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());

    // return this.angularFireAuth.auth.login({
    //   method: AuthMethods.Popup,
    //   provider: AuthProviders.Google,
    // });
  }

  public auth(): Observable<firebase.User> {
    return this.angularFireAuth.authState;
  }

  public profile(): Observable<IUserProfile> {
    return this.profileSubject.asObservable();
  }

  /**
   * Logs out the current user
   */
  public logout() {
    return this.angularFireAuth.auth.signOut();
  }

  public addToken(code: string): Observable<boolean> {
    const subject = new Subject();
    this.angularFireAuth.authState.subscribe((foo) => {
      this.http.post(environment.serverUrl + 'api/strava/auth', { code }).subscribe((x) => {

        const result: IStravaToken = x.json();

        const profile: IUserProfile = {
          email: foo.email,
          icon: foo.photoURL,
          strava_access_token: result.access_token,
          strava_athlete_id: result.athlete.id,
          username: foo.displayName,
        };

        this.angularFireDatabase.list('users/' + foo.uid).update('profile', profile)
          .then(() => {
            subject.next();
            subject.complete();
          }).catch((err) => {
            subject.error(err);
            subject.complete();
          });

      });

    });
    return subject.asObservable();
  }
}
