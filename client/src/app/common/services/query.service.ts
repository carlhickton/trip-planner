import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class QueryService {
    constructor(private http: Http) {
    }

    public getPointTypes(): Observable<string[]> {
        return this.http.get(environment.serverUrl + 'api/waypoint/types').map((x) => {
            const types: string[] = x.json();
            return types;
        });
    }

    public getPath(source: string): Observable<Array<GeoJSON.Feature<GeoJSON.LineString>>> {
        return this.http.get(environment.serverUrl + 'api/path?source=' + source).map((x) => {
            const types: Array<GeoJSON.Feature<GeoJSON.LineString>> = x.json();
            return types;
        });
    }

    public getWayPoints(source: string): Observable<Array<GeoJSON.Feature<GeoJSON.Point>>> {
        return this.http.get(environment.serverUrl + 'api/waypoint' + source).map((x) => {
            const types: Array<GeoJSON.Feature<GeoJSON.Point>> = x.json();
            return types;
        });
    }

}
