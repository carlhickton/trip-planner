import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from 'app/common/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';

// interface IGroupRoute {
//     routes: string[];
// }

export interface IGroupRoutes {
    values: string[];
}

@Injectable()
export class GroupService {

    private groupListSubject: ReplaySubject<string[]>;

    constructor(
        private authService: AuthService,
        private angularFireDatabase: AngularFireDatabase,
    ) {

        this.groupListSubject = new ReplaySubject(1);
        this.groupListSubject.next([]);

        this.authService.auth().subscribe((user) => {
            if (user !== null) {
                this.angularFireDatabase
                    .list('users/' + user.uid + '/routes/groups')
                    .subscribe((x: any[]) => {
                        const result: string[] = [];

                        x.forEach((element: any) => {
                            result.push(element.$key);
                        });
                        this.groupListSubject.next(result);
                    });
            } else {
                this.groupListSubject.next([]);
            }
        });

    }

    public saveGroup(name: string) {
        const subject = new Subject<void>();

        const route = {};
        route[name] = true;

        this.angularFireDatabase
            .list('users/' + this.authService.userId + '/routes')
            .update('groups', route)
            .then(() => { subject.next(); subject.complete(); })
            .catch((err) => subject.error(err));

        return subject.asObservable();
    }

    public listGroups(): Observable<string[]> {
        return this.groupListSubject.asObservable();
        // return this.angularFireDatabase
        //     .list('users/' + this.authService.userId + '/routes/groups')
        //     .map((x: FirebaseListObservable<any[]>) => {
        //         const result: string[] = [];

        //         x.forEach((element: any) => {
        //             result.push(element.$key);
        //         });
        //         return result;
        //     });
    }

    public addGroupPath(name: string, routeKey: string) {

        const subject = new Subject<void>();

        const group = {};
        group[routeKey] = true;

        const path = {};
        path[name] = true;

        this.angularFireDatabase
            .list('users/' + this.authService.userId + '/routes/groups')
            .update(name, group)
            .then(() => {
                this.angularFireDatabase
                    .list('users/' + this.authService.userId + '/routes/paths/' + routeKey)
                    .update('groups', path)
                    .then(() => { subject.next(); subject.complete(); })
                    .catch((err) => subject.error(err));
            })
            .catch((err) => subject.error(err));

        return subject.asObservable();
    }

    public removeGroupPath(name: string, routeKey: string) {

        const subject = new Subject<void>();

        const group = {};
        group[routeKey] = true;

        const path = {};
        path[name] = true;

        this.angularFireDatabase
            .object('users/' + this.authService.userId + '/routes/groups/' + name + '/' + routeKey)
            .remove()
            .then(() => {
                this.angularFireDatabase
                    .object('users/' + this.authService.userId + '/routes/paths/' + routeKey + '/groups/' + name)
                    .remove()
                    .then(() => { subject.next(); subject.complete(); })
                    .catch((err) => subject.error(err));
            })
            .catch((err) => subject.error(err));

        return subject.asObservable();
    }

    // public getGroupRoutes(groupName: string): Observable<any[]> { // Observable<IManualRoute[]> {

    //     // const subject = new Subject<IManualRoute[]>();

    //     // let results: IManualRoute[] = [];

    //     return this.angularFireDatabase
    //         .object('users/' + this.authService.userId + '/routes/groups/' + groupName)
    //         .map((group) => {
    //             // const routes = Observable.of(Object.keys(route));
    //             routes.map((route) => {
    //                 route.value = this.angularFireDatabase
    //                     .object('users/' + this.authService.userId + '/routes/paths/' + route.$key);
    //                 return route;
    //             });
    //             return routes;
    //         });
    // }

    public getGroupRoutes(groupName: string): Observable<IGroupRoutes> { // Observable<IManualRoute[]> {

        // const subject = new Subject<IManualRoute[]>();

        // let results: IManualRoute[] = [];

        return this.angularFireDatabase
            .object('users/' + this.authService.userId + '/routes/groups/' + groupName)
            .map((route: IGroupRoutes) => {
                // // const foo = Observable.of(Object.keys(routes));
                // routes.map((route) => {
                //     route.value = this.angularFireDatabase
                //         .object('users/' + this.authService.userId + '/routes/paths/' + route.$key);
                //     return route;
                // });

                route.values = Object.keys(route);

                return route;
            });
    }
}
