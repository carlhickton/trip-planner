import { Injectable } from '@angular/core';
import { ComponentType, MdSnackBar, MdSnackBarConfig, MdSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable()
export class NotifyService {

    private component: ComponentType<any>;

    constructor(public snackBar: MdSnackBar) { }

    public openSnackBar() {
        this.snackBar.openFromComponent<any>(this.component, {
            duration: 500,
        });
    }

    public showMessage(message: string): MdSnackBarRef<SimpleSnackBar> {

        const config  = new MdSnackBarConfig();
        config.announcementMessage = 'polite',
        config.duration = 3000;

        return this.snackBar.open(message, 'dismiss', config);
    }

    public setComponent(component: ComponentType<any>) {
        this.component = component;
    }
}
