import { Injectable } from '@angular/core';

import { Feature, LineString } from 'geojson';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/mergeMap';

import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from './auth.service';

export interface IProfileDevice {
    deviceId: string;
    deviceType: string;
    name: string;
}

export interface IDevice {
    deviceId: string;
    deviceType: string;
    userId: string;
    syncDate?: Date;
    routes?: Array<Feature<LineString>>;
}

export interface IListDevice {
    deviceId: string;
    deviceType: string;
    detail: IDevice;
}

export interface ISyncRoutes {
    name: string;
    type: string;
}

@Injectable()
export class DeviceService {
    constructor(
        private authService: AuthService,
        private angularFireDatabase: AngularFireDatabase,
        ) {
    }

    public listDevices(): Observable<IListDevice[]> {
        return this.angularFireDatabase
            .list('users/' + this.authService.userId + '/devices')
            .map((devices) => {
                devices.map((device) => {
                    device.detail = this.angularFireDatabase.object('/devices/' + device.deviceId);
                });
                return devices;
            })
            .map((devices) => {
                devices.map((device) => {
                    device.routes = this.angularFireDatabase.list('/devices/' + device.deviceId + '/routes');
                });
                return devices;
            });
    }

    public newDevice(name: string, id: string, type: string): Observable<any> {
        const result = new Subject();

        const profileDevice: IProfileDevice = {
            deviceId: id,
            deviceType: type,
            name,
        };

        const device: IDevice = {
            deviceId: id,
            deviceType: type,
            syncDate: null,
            userId: this.authService.userId,
        };

        this.angularFireDatabase
            .list('users/' + this.authService.userId + '/devices')
            .update(id, profileDevice).then(() => {

                this.angularFireDatabase
                    .list('devices')
                    .update(id, device).then(() => {
                        result.next();
                        result.complete();

                    }).catch((err) => {
                        result.error(err);
                    });
            }).catch((err) => {
                result.error(err);
            });

        return result;
    }

    public removeDevice(id: string) {
        console.log('remove device', id);
    }
}
