import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../environments/environment';

export interface IRouteSummary {
    description: string;
    distance: number;
    elevation: number;
    id: number;
    name: string;
    summaryPolyline: string;
}

@Injectable()
export class StravaService {
    constructor(private http: Http) {
    }

    public getRoutes(token: string, athleteId: number): Observable<IRouteSummary[]> {

        const result = new BehaviorSubject(JSON.parse(localStorage['routesCache'] || null));

        const body = {
            token,
            athleteId,
        };

        this.http.post(environment.serverUrl + 'api/strava/routes', body)
            .map((x) => {
                const types = x.json();
                return types;
            }).subscribe((data) => {
                result.next(data);
                localStorage['routesCache'] = JSON.stringify(data);
            });

        return result.asObservable();
    }

    public sendRoute(token: string, routeId: number, deviceId: string) {
        const result = new Subject();

        const body = {
            token,
            routeId,
            deviceId,
        };

        this.http.post(environment.serverUrl + 'api/strava/device', body)
            .subscribe((data) => {
                result.next(data);
            });

        return result.asObservable();
    }
}
