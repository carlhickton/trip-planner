import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { FirebaseApp } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AuthService } from 'app/common/services/auth.service';
import { environment } from 'environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { GroupService } from './group.service';

export interface IManualRoute {
    $key?: string;
    name: string;
    original: string;
    geoJson: string;
    summary: string;
    distance: number;
    elevation: number;
}

export interface IUploadRouteProgress {
    fileName: string;
    isParsing: boolean;
    isCompleate: boolean;
}

@Injectable()
export class RoutesService {
    constructor(
        private angularFireApp: FirebaseApp,
        private angularFireDatabase: AngularFireDatabase,
        private http: Http,
        private authService: AuthService,
        private groupService: GroupService) {
    }

    public listRoutes(): FirebaseListObservable<IManualRoute[]> {
        return this.angularFireDatabase
            .list('users/' + this.authService.userId + '/routes/paths');
    }

    public sendToDevice(deviceId: string, route: IManualRoute) {

        const subject = new Subject<boolean>();

        let objectRef = route.geoJson;
        if (!objectRef) {
            objectRef = route.original;
        }

        const request = {
            deviceId,
            name: route.name,
            objectRef,
        };

        this.http.post(environment.serverUrl + 'api/path/device', request)
            .subscribe(() => {
                subject.next(true);
                subject.complete();
            }, (err) => {
                subject.error(err);
                subject.complete();
            });

        return subject.asObservable();

    }

    public deleteRoute(route: IManualRoute): Observable<boolean> {
        const subject = new Subject<boolean>();
        const storageRef = this.angularFireApp.storage().ref('/');

        const removeRecord = () => this.angularFireDatabase
                        .object('users/' + this.authService.userId + '/routes/paths/' + route.$key)
                        .remove().then(() => {
                            subject.next(true);
                        }, (err) => {
                            subject.error(err);
                        });

        const removeGeoJson = () => {
            if (route.geoJson) {
                storageRef.child(route.geoJson).delete().then(() => {
                    removeRecord();
                }, (err) => {
                    // subject.error(err);
                    removeRecord();
                });
            } else {
                removeRecord();
            }
        };

        console.log(route.original);

        storageRef.child(route.original).delete().then(() => {
            removeGeoJson();
        }, (err: any) => {
            if (err.code === 'storage/object-not-found') {
                removeGeoJson();
            } else {
                subject.error(err);
            }

        });

        return subject.asObservable();
    }

    public getRouteByKey(key: string): FirebaseObjectObservable<IManualRoute> {
        return this.angularFireDatabase
            .object('users/' + this.authService.userId + '/routes/paths/' + key);
    }

    public getRouteData(objectRef: string): firebase.Promise<string> {
        const storageRef = this.angularFireApp.storage().ref('/');
        return storageRef.child(objectRef).getDownloadURL();
    }

    public saveFeature(
        name: string,
        feature: GeoJSON.Feature<GeoJSON.LineString>,
        groupName: string): Observable<IUploadRouteProgress> {
        console.log(this.authService.userId);

        const subject = new Subject();

        this.angularFireApp.database()
            .ref('users/' + this.authService.userId + '/routes/paths')
            .push(true)
            .then((x) => {

                const key = x.key;

                const storageRef = this.angularFireApp.storage().ref('/');
                const uploadTask = storageRef.child('/users/' + this.authService.userId)
                    .child(key + '.json')
                    .putString(JSON.stringify(feature), 'raw', { contentType: 'application/vnd.geo+json' });

                this.processNewRoute(storageRef, uploadTask, name, groupName, key).subscribe((y) => {
                    subject.next(y);
                });
            });

        return subject.asObservable();
    }

    public saveFiles(files: FileList, groupName: string): Observable<IUploadRouteProgress> {

        const uploadState: IUploadRouteProgress = {
            fileName: '',
            isCompleate: false,
            isParsing: false,
        };

        const saveFileResult = new BehaviorSubject<IUploadRouteProgress>(uploadState);

        // const filesObs = Observable.of(files);

        // filesObs.subscribe(f => {
        //     this.saveFile(f, groupName).subscribe((x) => {
        //         saveFileResult.next(x);
        //     });
        // });

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < files.length; i++) {
            this.saveFile(files[i], groupName).subscribe((x) => {
                saveFileResult.next(x);
            });
        }

        // this.saveFile(files[0], groupName).subscribe((x) => {
        //     saveFileResult.next(x);
        // });

        return saveFileResult.asObservable();
    }

    private saveFile(file: File, groupName: string): Observable<IUploadRouteProgress> {

        const subject = new Subject();

        this.angularFireApp.database()
            .ref('users/' + this.authService.userId + '/routes/paths')
            .push(true)
            .then((x) => {

                const key = x.key;

                const fileName = key + '.' + file.name.split('.').pop();
                const storageRef: firebase.storage.Reference = this.angularFireApp.storage().ref('/');
                const uploadTask = storageRef.child('/users/' + this.authService.userId).child(fileName).put(file);

                this.processNewRoute(storageRef, uploadTask, fileName, groupName, key).subscribe((y) => {
                    subject.next(y);
                });

            });

        return subject.asObservable();
    }

    private processNewRoute(
        storageRef: firebase.storage.Reference,
        uploadTask: firebase.storage.UploadTask,
        name: string,
        groupName: string,
        key: string) {

        const uploadState: IUploadRouteProgress = {
            fileName: name,
            isCompleate: false,
            isParsing: false,
        };

        const saveFileResult = new BehaviorSubject<IUploadRouteProgress>(uploadState);

        uploadTask.then((uploadTaskSnapshot: firebase.storage.UploadTaskSnapshot) => {

            uploadState.isParsing = true;
            saveFileResult.next(uploadState);

            console.log(uploadTaskSnapshot.metadata.fullPath);

            const body = { objectRef: uploadTaskSnapshot.metadata.fullPath };

            this.http.post(environment.serverUrl + 'api/path/parse', body).subscribe((response) => {
                const result = response.json();

                let newName = name;

                if (result.name) {
                    newName = result.name;
                }

                this.createRoute(key, this.authService.userId, {
                    distance: result.distance,
                    elevation: result.elevation,
                    geoJson: result.geoJsonRef,
                    name: newName,
                    original: uploadTaskSnapshot.metadata.fullPath,
                    summary: result.summaryMap,
                }).subscribe((newKey: string) => {

                    this.groupService.addGroupPath(groupName, newKey).subscribe(() => {
                        uploadState.isCompleate = true;
                        saveFileResult.next(uploadState);
                    });

                    // this.notifyService.showMessage('File Saved');
                }, (err) => {
                    storageRef.child('/users/' + this.authService.userId).child(name).delete().then((x) => {
                        saveFileResult.error(err);
                    });

                });

            }, (err) => {
                storageRef.child('/users/' + this.authService.userId).child(name).delete().then((x) => {
                    saveFileResult.error(err);
                });
            });
        });

        return saveFileResult.asObservable();
    }

    private createRoute(key: string, userId: string, route: IManualRoute): Observable<string> {
        const subject = new Subject<string>();
        this.angularFireDatabase
            .list('users/' + userId + '/routes/paths')
            // .push(route)
            .update(key, route)
            .then((data) => {
                subject.next(key);
                subject.complete();
            }).catch((err) => {
                subject.error(err);
            });

        return subject.asObservable();
    }
}
