import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { MapBounds } from './map-bounds.model';

@Injectable()
export class MapService {

    private boundsSubject: BehaviorSubject<MapBounds>;

    private bounds: MapBounds;

    constructor() {
        this.boundsSubject = new BehaviorSubject<MapBounds>(null);
        this.bounds = new MapBounds();
    }

    public Bounds(): Observable<MapBounds> {
        return this.boundsSubject.asObservable()
            .filter((bounds) => bounds !== null);
        // .debounceTime(2000);
    }

    public setMapBounds(north: number, south: number, east: number, west: number) {
        this.bounds.north = north;
        this.bounds.south = south;
        this.bounds.east = east;
        this.bounds.west = west;

        this.boundsSubject.next(this.bounds);
    }

    public setTypes(types: string) {
        this.bounds.types = types;

        this.boundsSubject.next(this.bounds);
    }
}
