export class MapBounds {
    public north: number;
    public south: number;
    public east: number;
    public west: number;
    public types: string;

    constructor() {
        this.types = '';
    }

    public toQueryString() {
        return '?north=' + this.north +
        '&south=' + this.south +
        '&west=' + this.west +
        '&east=' + this.east +
        '&types=' + this.types;
    }
}
