import { Component, OnDestroy, ViewChild } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MdSidenav } from '@angular/material';
import { Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { environment } from '../environments/environment';
import { AuthService, IUserProfile } from './common/services/auth.service';
import { GroupService } from './common/services/group.service';
import { NotifyService } from './common/services/notify.service';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
})
export class AppComponent implements OnDestroy {
  public isLoggedIn: boolean;
  public hasStrava: boolean;

  public filterForm: FormGroup;
  public showForm = false;
  public critera: string;
  public formChanges: Subscription;

  public stravaLoginUrl: string;

  public pageType: string;

  @ViewChild('sidenav')
  public sidenav: MdSidenav;

  public groups: string[];

  public userProfileIcon: string;

  private userProfile: IUserProfile;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    public authService: AuthService,
    private router: Router,
    private notifyService: NotifyService,
    private groupService: GroupService) {

    this.critera = null;
    this.hasStrava = false;

    this.stravaLoginUrl = 'https://www.strava.com/oauth/authorize?client_id=2437&response_type=code';
    this.stravaLoginUrl += '&redirect_uri=' + environment.hostUrl + 'auth/strava';
    this.stravaLoginUrl += '&scope=view_private&approval_prompt=force';

    this.router.events
      .takeUntil(this.ngUnsubscribe)
      .subscribe(() => {
        if (this.sidenav.opened) {
          this.sidenav.close();
        }
      });

    this.authService.auth()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
      (auth) => {
        if (auth == null) {
          this.isLoggedIn = false;
          this.hasStrava = false;
          this.userProfileIcon = null;
          this.notifyService.showMessage('Not Logged In');
        } else {
          this.isLoggedIn = true;
          this.notifyService.showMessage('You are Authencated');
        }
      });

    this.authService.profile()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.userProfile = x;
        if (x !== null) {
          this.userProfileIcon = 'url(' + x.icon + ')';
          if (x.strava_access_token !== undefined && x.strava_access_token !== null) {
            this.hasStrava = true;
          } else {
            this.hasStrava = false;
          }
        }
      });

    this.groupService.listGroups()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((groups) => {
        this.groups = groups;
      });
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public maps() {
    this.sidenav.close();

    this.sidenav.onClose.take(1).subscribe((x) => {
      this.router.navigate(['map']);
    });
  }

  public mapRoute() {
    this.sidenav.close();

    this.sidenav.onClose.take(1).subscribe((x) => {
      this.router.navigate(['map/route']);
    });

  }

  public logout() {
    this.authService.logout();
  }

  get typesList(): FormArray {
    return this.filterForm.get('typesList') as FormArray;
  }
}
