import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
    MdButtonModule,
    MdCheckboxModule,
    MdDialogModule,
    MdIconModule,
    MdInputModule,
    MdProgressBarModule,
    MdSelectModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';

import { AgmCoreModule } from 'angular2-google-maps/core';
import { DataLayerDirective } from 'app/map/directives/data-layer.directive';
import { environment } from '../../environments/environment';
import { TripCommonModule } from '../common/common.module';
import { MdFabSpeedDialModule } from '../material/index';
import { NewRouteDirective } from './directives/new-route.directive';
import { PlacesLayerDirective } from './directives/places-layer.directive';
import { RouteLayerDirective } from './directives/route-layer.directive';
import { MapComponent } from './pages/map/map.component';
import { MapLayersDialogComponent } from './pages/route/map-layers-dialog/map-layers-dialog.component';
import { RouteComponent } from './pages/route/route.component';
import { SaveRouteDialogComponent } from './pages/route/save-route-dialog/save-route-dialog.component';
import { MapRoutesService } from './services/map-routing.service';

@NgModule({
    declarations: [
        MapComponent,
        RouteComponent,
        DataLayerDirective,
        NewRouteDirective,
        RouteLayerDirective,
        PlacesLayerDirective,
        SaveRouteDialogComponent,
        MapLayersDialogComponent,
    ],
    entryComponents: [
        SaveRouteDialogComponent,
        MapLayersDialogComponent,
    ],
    exports: [],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                component: MapComponent,
                data: { title: 'Routes', type: 'map' },
                path: '',
            },
            {
                component: RouteComponent,
                data: { title: 'Create Route', type: 'map' },
                path: 'route',
            },
        ]),
        AgmCoreModule.forRoot({
            apiKey: environment.googleMapApi,
            libraries: ['places'],
        }),

        MdIconModule,
        MdButtonModule,
        MdInputModule,
        MdDialogModule,
        MdProgressBarModule,
        MdSelectModule,
        MdCheckboxModule,
        // MdTooltipModule,
        MdFabSpeedDialModule,
        TripCommonModule,
    ],
    providers: [
        MapRoutesService,
    ],
})
export class MapModule {
}
