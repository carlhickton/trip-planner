import { Directive, Input, OnChanges, OnDestroy, OnInit, SimpleChange } from '@angular/core';

import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { GroupService } from 'app/common/services/group.service';
import { IManualRoute, RoutesService } from 'app/common/services/routes.service';

@Directive({
    selector: 'route-layer',
})
export class RouteLayerDirective implements OnInit, OnDestroy, OnChanges {

    @Input()
    public visible: boolean;

    @Input()
    public color: string;

    @Input()
    public groupName: string;

    private dataLayer: google.maps.Data;
    private map: google.maps.Map;

    private features: google.maps.Data.Feature[][];

    private mouseOutListner: google.maps.MapsEventListener;

    private isLoaded: boolean = false;

    constructor(
        private wrapper: GoogleMapsAPIWrapper,
        private groupService: GroupService,
        private routesService: RoutesService) {
        this.features = [];
        this.visible = false;
    }

    public ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (this.isLoaded) {
            const changedInputs = Object.keys(changes);
            if (changedInputs.indexOf('visible') !== -1 || changedInputs.indexOf('visible') !== -1) {
                this.cleanUp();
                this.loadData();
            }
        }
    }

    public ngOnInit() {
        this.wrapper.getNativeMap().then((map: any) => {
            this.map = map;
            this.dataLayer = new google.maps.Data();

            const infoWindow = new google.maps.InfoWindow();

            this.dataLayer.setStyle((feature) => {
                const color = (this.color) ? this.color : 'gray';
                return ({
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                });
            });

            this.dataLayer.addListener('click', (event: google.maps.Data.MouseEvent) => {

                const name = event.feature.getProperty('name');
                const distance = (+event.feature.getProperty('distance') / 1000).toFixed(2);
                const elevation = (+event.feature.getProperty('elevation')).toFixed(2);

                const constent = `<h2>Name: ${name}</h2>Distance: ${distance} km<br>Elevation: ${elevation} m<br>Group: ${this.groupName}`;
                infoWindow.setContent(constent);
                infoWindow.setPosition(event.latLng);
                infoWindow.open(this.map);
            });

            this.dataLayer.addListener('mouseover', (event: google.maps.Data.MouseEvent) => {
                this.dataLayer.revertStyle();
                this.dataLayer.overrideStyle(event.feature, { strokeWeight: 4 });
            });

            this.mouseOutListner = this.dataLayer.addListener('mouseout', () => {
                this.dataLayer.revertStyle();
            });

            this.dataLayer.setMap(this.map);

            this.isLoaded = true;

            this.loadData();

        });
    }

    public ngOnDestroy(): void {
        console.log('distory layer');
        this.cleanUp();
        // this.features.forEach((x) => {
        //     google.maps.event.clearInstanceListeners(x);
        //     this.dataLayer.remove(x);
        // });

    }

    private cleanUp() {
        this.features.forEach((featureCollection) => {
            featureCollection.forEach((x) => {
                google.maps.event.clearInstanceListeners(x);
                this.dataLayer.remove(x);
            });

        });
    }

    private loadData() {
        if (this.isLoaded && this.visible) {
            this.groupService.getGroupRoutes(this.groupName).take(1)
                .subscribe((x) => {

                    this.cleanUp();

                    x.values.forEach((key) => {
                        this.routesService.getRouteByKey(key)
                            .take(1)
                            .subscribe((y: IManualRoute) => {
                                let objectRef = y.geoJson;
                                if (!objectRef) {
                                    objectRef = y.original;
                                }

                                this.routesService.getRouteData(objectRef).then((url) => {
                                    this.dataLayer.loadGeoJson(url, null, ((features) => {
                                        // console.log(features[0]);
                                        this.features.push(features);
                                    }));
                                });

                            });
                    });

                });
        }
    }
}
