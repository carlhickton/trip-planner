import { Directive, Input, OnDestroy, OnInit } from '@angular/core';

import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { Subject } from 'rxjs/Subject';
import { QueryService } from '../../common/services/query.service';

@Directive({
    selector: 'data-layer',
})
export class DataLayerDirective implements OnInit, OnDestroy {
    @Input()
    public source: string;

    @Input()
    public color: string;
    @Input()
    public selectedColor: string;

    private dataLayer: google.maps.Data;
    private map: google.maps.Map;
    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(private wrapper: GoogleMapsAPIWrapper, private queryService: QueryService) {
    }

    public ngOnInit() {
        console.log('source', this.source);

        this.wrapper.getNativeMap().then((map: any) => {
            this.map = map;
            this.dataLayer = new google.maps.Data();

            this.queryService.getPath(this.source)
                .takeUntil(this.ngUnsubscribe)
                .subscribe((features) => {

                    const f: GeoJSON.FeatureCollection<GeoJSON.LineString> = {
                        type: 'FeatureCollection',
                        features,
                    };

                    this.dataLayer.addGeoJson(f);

                    this.dataLayer.setStyle((feature) => {
                        let color = (this.color) ? this.color : 'gray';
                        if (feature.getProperty('isColorful')) {
                            color = (this.selectedColor) ? this.selectedColor : 'green';
                        }
                        return ({
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                        });
                    });

                    this.dataLayer.addListener('click', (event: google.maps.Data.MouseEvent) => {
                        if (!event.feature.getProperty('isColorful')) {
                            event.feature.setProperty('isColorful', true);
                        } else {
                            event.feature.setProperty('isColorful', false);
                        }
                    });

                    this.dataLayer.addListener('mouseover', (event: google.maps.Data.MouseEvent) => {
                        this.dataLayer.revertStyle();
                        this.dataLayer.overrideStyle(event.feature, { strokeWeight: 4 });
                    });

                    this.dataLayer.addListener('mouseout', () => {
                        this.dataLayer.revertStyle();
                    });

                    this.dataLayer.setMap(this.map);

                });

        });
    }

    public ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
