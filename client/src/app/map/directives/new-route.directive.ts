import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';

import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

@Component({
    selector: 'new-route',
    template: '<div class="geojson-info-window-content"><ng-content></ng-content></div>',
})
export class NewRouteDirective implements OnInit, OnDestroy {

    @Input()
    public feature: GeoJSON.Feature<GeoJSON.LineString>;

    @Input()
    public color: string;

    @Input()
    public name: string;

    private dataLayer: google.maps.Data;
    private map: google.maps.Map;

    private features: google.maps.Data.Feature[];

    private mouseOutListner: google.maps.MapsEventListener;

    constructor(private wrapper: GoogleMapsAPIWrapper, private element: ElementRef) {
    }

    public ngOnInit() {
        this.wrapper.getNativeMap().then((map: any) => {
            this.map = map;
            this.dataLayer = new google.maps.Data();

            const infowindow = new google.maps.InfoWindow({
                content: this.element.nativeElement.querySelector('.geojson-info-window-content'),
            });

            const features = Array<GeoJSON.Feature<GeoJSON.LineString>>();
            features.push(this.feature);

            const f: GeoJSON.FeatureCollection<GeoJSON.LineString> = {
                features,
                type: 'FeatureCollection',
            };

            this.features = this.dataLayer.addGeoJson(f);

            this.dataLayer.setStyle((feature) => {
                const color = (this.color) ? this.color : 'gray';
                return ({
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                });
            });

            this.dataLayer.addListener('click', (event: google.maps.Data.MouseEvent) => {
                infowindow.setPosition(event.latLng);
                infowindow.open(this.map);
            });

            this.dataLayer.addListener('mouseover', (event: google.maps.Data.MouseEvent) => {
                this.dataLayer.revertStyle();
                this.dataLayer.overrideStyle(event.feature, { strokeWeight: 4 });
            });

            this.mouseOutListner = this.dataLayer.addListener('mouseout', () => {
                this.dataLayer.revertStyle();
            });

            this.dataLayer.setMap(this.map);

        });
    }

    public ngOnDestroy(): void {

        this.features.forEach((x) => {
            google.maps.event.clearInstanceListeners(x);
            this.dataLayer.remove(x);
        });

    }
}
