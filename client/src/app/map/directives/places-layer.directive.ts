import { Directive, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

export class IGooglePlace {
    public placeId: string;
    public name: string;
    public icon: any;
    public lat: number;
    public lng: number;
}

// tslint:disable-next-line:max-classes-per-file
@Directive({
    selector: 'places-layer',
})
export class PlacesLayerDirective implements OnInit, OnDestroy {

    @Input()
    public lat: number;

    @Input()
    public lng: number;

    @Input()
    public searchType: string;

    @Output()
    public onLoadPlaces = new EventEmitter<IGooglePlace[]>();

    private infoWindow: google.maps.InfoWindow;
    private map: google.maps.Map;
    private searchRadius: google.maps.Circle;
    private isLoaded: boolean = false;

    constructor(
        private wrapper: GoogleMapsAPIWrapper) {
    }

    public ngOnInit() {
        this.wrapper.getNativeMap().then((map: any) => {
            this.map = map;

            this.infoWindow = new google.maps.InfoWindow();

            const possion = { lat: this.lat, lng: this.lng };

            this.searchRadius = new google.maps.Circle({
                center: new google.maps.LatLng(this.lat, this.lng),
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                radius: 10000,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                map,
            });

            const service = new google.maps.places.PlacesService(map);
            service.nearbySearch({
                location: possion,
                radius: 10000,
                type: this.searchType,
            }, (
                results: google.maps.places.PlaceResult[],
                status: google.maps.places.PlacesServiceStatus,
                pagination: google.maps.places.PlaceSearchPagination) => {
                    // console.log(results);
                    this.callback(results, status, pagination);
                });

            // service.nearbySearch({
            //     location: possion,
            //     radius: 10000,
            //     type: 'bicycle_store',
            // }, (
            //     results: google.maps.places.PlaceResult[],
            //     status: google.maps.places.PlacesServiceStatus,
            //     pagination: google.maps.places.PlaceSearchPagination) => {
            //         // console.log(results);
            //         this.callback(results, status, pagination);
            //     });

            this.isLoaded = true;

        });
    }

    public ngOnDestroy(): void {
        console.log('distory layer');
    }

    private callback(
        results: google.maps.places.PlaceResult[],
        status: google.maps.places.PlacesServiceStatus,
        pagination: google.maps.places.PlaceSearchPagination) {

        setTimeout(() => {
            this.searchRadius.setMap(null);
        }, 3000);

        if (status === google.maps.places.PlacesServiceStatus.OK) {

            const places: IGooglePlace[] = [];

            for (const result of results) {
                // this.createMarker(result);

                const gpmarker: google.maps.Icon = {
                    scaledSize: new google.maps.Size(25, 25),
                    url: result.icon,

                    // place.icon, null, null, null, new google.maps.Size(25, 25))
                };

                places.push({
                    icon: (gpmarker),  // result.icon,
                    lat: result.geometry.location.lat(),
                    lng: result.geometry.location.lng(),
                    name: result.name,
                    placeId: result.place_id,
                });
            }

            if (pagination.hasNextPage) {
                pagination.nextPage();
            }

            this.onLoadPlaces.emit(places);
        }
    }

    // private createMarker(place) {
    //     // const placeLoc = place.geometry.location;

    //     // const gpmarker = new google.maps.MarkerImage(place.icon, null, null, null, new google.maps.Size(25, 25));

    //     const gpmarker: google.maps.Icon = {
    //         scaledSize: new google.maps.Size(25, 25),
    //         url: place.icon,

    //         // place.icon, null, null, null, new google.maps.Size(25, 25))
    //     };

    //     const marker = new google.maps.Marker({
    //         icon: gpmarker,
    //         map: this.map,
    //         position: place.geometry.location,
    //     });

    //     google.maps.event.addListener(marker, 'click', (event: google.maps.MouseEvent) => {
    //         const constent = `Name: ${place.name}`;
    //         this.infoWindow.setContent(constent);
    //         this.infoWindow.setPosition(event.latLng);
    //         this.infoWindow.open(this.map);
    //     });
    // }
}
