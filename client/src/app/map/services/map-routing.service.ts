import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';

export interface IMapPoint {
    lat: number;
    lng: number;
    label: string;
    index: number;
}

export interface ISearchMapPoint {
    lat: number;
    lng: number;
    searchType: string;
}

export interface IMapRoute {
    color: string;
    feature: GeoJSON.Feature<GeoJSON.LineString>;
    source: string;
    visible: boolean;
}

@Injectable()
export class MapRoutesService {
    constructor(private http: Http) {
    }

    public getFeatures(points: IMapPoint[], source: string): Observable<Array<GeoJSON.Feature<GeoJSON.LineString>>> {
        return this.http.post(environment.serverUrl + 'api/path/feature', { points, source })
            .map((x) => {
                return x.json();
            });
    }

    // public searchPoint(lat: number, lng: number) {

    //     let query = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
    //     query += '?location=' + lat + ',' + lng;

    //     query += '&radius=40000&type=campground&key=' + environment.googleMapApi;

    //     this.http.get(query)
    //         .subscribe(x => {
    //             const resp = x.json();
    //             console.log(resp);
    //         })
    //     // .map((x) => {
    //     //     return x.json();
    //     // });
    // }
}
