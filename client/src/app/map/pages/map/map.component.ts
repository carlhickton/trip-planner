import { Component, OnDestroy, OnInit } from '@angular/core';

import { LatLngBounds } from 'angular2-google-maps/core';
import { MapService } from 'app/common/services/map.service';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { QueryService } from '../../../common/services/query.service';

// import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'map-page',
  styleUrls: ['./map.component.scss'],
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit, OnDestroy {

  public zoom: number;
  public lat: number;
  public lng: number;

  public points: Array<GeoJSON.Feature<GeoJSON.Point>> = [];
  public paths: Array<GeoJSON.Feature<GeoJSON.LineString>> = [];

  public innerHeight: string;

  private pointSubscription: Subscription;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private mapService: MapService, private queryService: QueryService) {
    this.loadMapLocation();

    const height = Math.max(
        window.innerHeight,
        document.body.offsetHeight,
        document.documentElement.clientHeight);

    this.innerHeight = (height - 65) + 'px';
  }

  public loadMapLocation() {
    this.lat = +localStorage.getItem('map-lat');
    this.lng = +localStorage.getItem('map-lng');
    this.zoom = +localStorage.getItem('map-zoom');

    if (this.lat === 0) {
      this.lat = 41.890949999999997;
    }
    if (this.lng === 0) {
      this.lng = 12.51613;
    }
    if (this.zoom === 0) {
      this.zoom = 11;
    }
  }

  public ngOnInit() {

    this.mapService.Bounds()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((query) => {

        if (this.pointSubscription) {
          this.pointSubscription.unsubscribe();
        }

        this.pointSubscription = this.queryService.getWayPoints(query.toQueryString())
          .takeUntil(this.ngUnsubscribe)
          .subscribe((points) => {
            this.points = points;
          }, (err) => {
            console.log('waypoints ', err);
          });
      });
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public onZoomChange(event: number) {
    localStorage.setItem('map-zoom', event.toString());
  }

  public onBoundsChange(event: LatLngBounds) {

    const center = event.getCenter();
    this.saveMapLocation(center.lat(), center.lng());

    this.mapService.setMapBounds(
      event.getNorthEast().lat(),
      event.getSouthWest().lat(),
      event.getNorthEast().lng(),
      event.getSouthWest().lng());
  }

  public identify<T extends GeoJSON.GeometryObject>(_: number, item: GeoJSON.Feature<T>) {
    return item.id;
  }

  public getIcon(icon: string): string {

    let newIcon = null;

    switch (icon.toLowerCase().trim()) {
      case 'campground':
        newIcon = '/assets/campsite.png';
        break;
      case 'information':
        newIcon = '/assets/information.png';
        break;
      case 'railway':
      case 'ground transportation':
        newIcon = '/assets/railway.png';
        break;
      case 'restaurant':
      case 'bar':
        newIcon = '/assets/cafe.png';
        break;
      case 'picnic area':
        newIcon = '/assets/picnicarea.png';
        break;
      case 'scenic area':
      case 'skull and crossbones':
      case 'museum':
        newIcon = '/assets/scenicarea.png';
        break;
      case 'drinking water':
        newIcon = '/assets/water.png';
        break;
      case 'restroom':
        newIcon = '/assets/restroom.png';
        break;
      case 'bike trail':
        newIcon = '/assets/bikepath.png';
        break;
      case 'lodging':
      case 'tunnel':
      case 'bridge':
      case 'custom 1':
      case 'pin, blue':
      case 'flag, red':
        newIcon = null;
        break;
      default:
        console.log(icon);
    }
    return newIcon;
  }

  private saveMapLocation(lat: number, lng: number) {
    localStorage.setItem('map-lat', lat.toString());
    localStorage.setItem('map-lng', lng.toString());
  }

}
