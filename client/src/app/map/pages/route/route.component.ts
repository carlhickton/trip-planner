import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MdDialog } from '@angular/material';

import { LatLngBounds, MouseEvent } from 'angular2-google-maps/core';
import { NotifyService } from 'app/common/services/notify.service';
import { Feature, LineString } from 'geojson';
import { Subject } from 'rxjs/Subject';
import { IMapPoint, IMapRoute, ISearchMapPoint, MapRoutesService } from '../../services/map-routing.service';
import { SaveRouteDialogComponent } from './save-route-dialog/save-route-dialog.component';

import { GroupService } from 'app/common/services/group.service';
import * as _ from 'lodash';
import { MdFabSpeedDialComponent } from '../../../material/fab-speed-dial.component';
import { IGooglePlace } from '../../directives/places-layer.directive';
import { MapLayersDialogComponent } from './map-layers-dialog/map-layers-dialog.component';

export interface ISelectedGroups {
  name: string;
  selected: boolean;
}

@Component({
  selector: 'route-page',
  styleUrls: ['./route.component.scss'],
  templateUrl: './route.component.html',
})
export class RouteComponent implements OnDestroy, OnInit {

  public zoom: number;
  public lat: number;
  public lng: number;

  public groups: ISelectedGroups[];

  public isProcessing: boolean;

  @ViewChild('myFab')
  public myFab: MdFabSpeedDialComponent;

  public points: IMapPoint[] = [];

  public searchPoints: ISearchMapPoint[] = [];

  public features: IMapRoute[];

  public innerHeight: string;

  public places: IGooglePlace[];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private counter: number;

  private colours: string[];

  constructor(
    private mapRoutesService: MapRoutesService,
    private notifyService: NotifyService,
    private dialog: MdDialog,
    private groupService: GroupService) {

    const height = Math.max(
      window.innerHeight,
      document.body.offsetHeight,
      document.documentElement.clientHeight);

    this.innerHeight = (height - 65) + 'px';
    this.loadMapLocation();
    this.counter = 1;
    this.features = [];
    this.colours = ['green', 'purple', 'gray', 'orange', 'pink'];
    this.isProcessing = false;

    this.groups = [];
    this.restorePlaces();
  }

  public createRoute() {
    this.clearFeatures();

    if (this.points.length < 2) {
      this.notifyService.showMessage('Need a Start and End Point');
      return;
    }

    this.isProcessing = true;

    this.mapRoutesService.getFeatures(this.points, 'cycle')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((results) => {
        this.addLayer(results, 'cycle', 'red');
      }, (err) => this.mapLayerError(err));

    this.mapRoutesService.getFeatures(this.points, 'cycle-road')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((results) => {
        this.addLayer(results, 'cycle-road', 'blue');
      }, (err) => this.mapLayerError(err));

    this.mapRoutesService.getFeatures(this.points, 'cycle-map')
      .takeUntil(this.ngUnsubscribe)
      .subscribe((results) => {
        this.addLayer(results, 'cycle-map');
      }, (err) => this.mapLayerError(err));
  }

  public loadPlaces(event: IGooglePlace[]) {
    this.places = _.unionBy(this.places, event, 'placeId');
    this.savePlaces();
  }

  public saveRoute(route: IMapRoute) {
    console.log('Saving', route);
    const dialog = this.dialog.open(SaveRouteDialogComponent);
    dialog.componentInstance.route = route;
  }

  public editLayers() {
    const dialog = this.dialog.open(MapLayersDialogComponent);
    dialog.componentInstance.groups = this.groups;

    // dialog.afterClosed().take(1).subscribe((x) => {
    //   console.log(x);
    //   this.groups = x;
    // });
  }

  public getColor(route: IMapRoute, i: number): string {
    if (route.color) {
      return route.color;
    }
    return this.colours[i];
  }

  public loadMapLocation() {
    this.lat = +localStorage.getItem('map-lat');
    this.lng = +localStorage.getItem('map-lng');
    this.zoom = +localStorage.getItem('map-zoom');

    if (this.lat === 0) {
      this.lat = 41.890949999999997;
    }
    if (this.lng === 0) {
      this.lng = 12.51613;
    }
    if (this.zoom === 0) {
      this.zoom = 11;
    }
  }

  public onZoomChange(event: number) {
    localStorage.setItem('map-zoom', event.toString());
  }

  public onBoundsChange(event: LatLngBounds) {
    const center = event.getCenter();
    this.saveMapLocation(center.lat(), center.lng());
  }

  public ngOnInit() {
    this.groupService.listGroups()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.groups = [];
        x.forEach((i) => {
          this.groups.push({
            name: i,
            selected: false,
          });
        });
      });
  }

  public trackByGroup(group: ISelectedGroups) {
    return group.name;
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();

    this.savePlaces();
  }

  public createPoint(event: MouseEvent) {
    this.insertPoint(event.coords.lat, event.coords.lng);
  }

  public createPointAtMyLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.insertPoint(position.coords.latitude, position.coords.longitude);
    });
  }

  public removePoint(event: IMapPoint) {
    this.clearFeatures();
    _.remove(this.points, (point) => {
      return point.index === event.index;
    });
  }

  public searchPoint(event: IMapPoint, searchType: string) {
    this.searchPoints.push({
      lat: event.lat,
      lng: event.lng,
      searchType,
    });
  }

  public clearMap() {
    this.points = [];
    this.counter = 1;
    this.clearFeatures();
  }

  private insertPoint(lat: number, lng: number) {
    const point: IMapPoint = {
      index: this.counter,
      label: this.counter.toString(),
      lat,
      lng,
    };

    ++this.counter;

    this.points.push(point);
  }

  private clearFeatures() {
    this.features = [];
  }

  private addLayer(features: Array<Feature<LineString>>, source: string, color?: string) {
    this.isProcessing = false;
    features.forEach((feature, index) => {
      if (!feature) {
        return;
      }
      this.features.push({
        color,
        feature,
        source: source + '-' + index,
        visible: true,
      });
    });
  }

  private mapLayerError(err) {
    this.isProcessing = false;
    console.log(err);
  }

  private saveMapLocation(lat: number, lng: number) {
    localStorage.setItem('map-lat', lat.toString());
    localStorage.setItem('map-lng', lng.toString());
  }

  private restorePlaces() {
    this.places = JSON.parse(localStorage['placesCache'] || null) || [];
  }

  private savePlaces() {
    localStorage['placesCache'] = JSON.stringify(_.takeRight(this.places, 100));
  }

}
