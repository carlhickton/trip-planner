import { Component, OnDestroy, OnInit } from '@angular/core';
import { MdCheckboxChange, MdDialogRef } from '@angular/material';

import { Subject } from 'rxjs/Subject';
import { ISelectedGroups } from '../route.component';

@Component({
  selector: 'map-layers-dialog',
  templateUrl: './map-layers-dialog.component.html',
})
export class MapLayersDialogComponent implements OnInit, OnDestroy {

  public groups: ISelectedGroups[];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<MapLayersDialogComponent>) {
    this.groups = [];
  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px', '400px');

    console.log('dialof init', this.groups);
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public saveRoute() {
    this.dialogRef.close(this.groups);
  }

  public updateGroup(event: MdCheckboxChange , group: ISelectedGroups) {
    // console.log(event, group);
    group.selected = event.checked;
  }

}
