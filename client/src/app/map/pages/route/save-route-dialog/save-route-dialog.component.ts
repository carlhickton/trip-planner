import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MdDialogRef } from '@angular/material';

import { GroupService } from 'app/common/services/group.service';
import { NotifyService } from 'app/common/services/notify.service';
import { RoutesService } from 'app/common/services/routes.service';
import { Subject } from 'rxjs/Subject';
import { IMapRoute } from '../../../services/map-routing.service';

interface IForm {
  name: string;
  groupName: string;
}

@Component({
  selector: 'save-route-dialog',
  templateUrl: './save-route-dialog.component.html',
})
export class SaveRouteDialogComponent implements OnInit, OnDestroy {
  public routeForm: FormGroup;
  public isProcessing: boolean;

  public route: IMapRoute;

  public groups: string[];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialogRef: MdDialogRef<SaveRouteDialogComponent>,
    private routesService: RoutesService,
    private notifyService: NotifyService,
    private groupService: GroupService) {

    this.isProcessing = false;

    this.groups = [];

    this.routeForm = new FormGroup({
      groupName: new FormControl(),
      name: new FormControl(),
    });

  }

  public ngOnInit(): void {
    this.dialogRef.updateSize('400px');

    this.groupService.listGroups()
      .takeUntil(this.ngUnsubscribe)
      .subscribe((x) => {
        this.groups = x;
      });

    console.log('dialof init', this.route);
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public saveRoute() {
    if (!this.routeForm.invalid) {

      this.isProcessing = true;

      const value: IForm = this.routeForm.value;

      this.routesService.saveFeature(value.name, this.route.feature, value.groupName).subscribe((r) => {
        if (r.isCompleate) {
          this.isProcessing = false;
          this.notifyService.showMessage('Saved route');
          this.dialogRef.close(true);
        }
      }, (err) => {
        this.isProcessing = false;
        this.notifyService.showMessage('Error saving route');
      });

    }
  }

}
